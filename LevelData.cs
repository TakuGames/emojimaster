﻿using UnityEngine;

public class LevelData : ScriptableObject
{
    [SerializeField] private float _speed;

    public float speed
    {
        get => _speed;
    }
}