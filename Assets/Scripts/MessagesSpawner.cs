﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessagesSpawner : MonoBehaviour
{
    [Header("[SPAWN_PARAMETERS]")]

    [SerializeField] private AnimationCurve _curve = null;

    [SerializeField] [Range(0, 100)] private int _chanceBadMessages = 0;
    [SerializeField] [Range(0, 5)] private int _countOfSkip = 2;
    [SerializeField] [Range(0, 10)] private int _startCount = 4;
    [SerializeField] [Range(0, 5)] private float _stepMessanges = 0.25f;
    [SerializeField] [Range(0, 5)] private float _stepEdge = 0.25f;
    [SerializeField] [Range(0, 5)] private float _speedOfLevel = 2.0f;

    [Header("[INITIALIZATION_PARAMETERS]")]

    [SerializeField] private Transform _messagesParent = null;

    private EDirection _currentDir = EDirection.Right;
    private EDirection _prevDir = EDirection.None;

    private float _wightBackground, _hightBackground;
    private float _wightInstance, _hightInstance;
    private float _countOfBlocks;
    private float _startLevelSpeed;

    private bool _starting = true;

    private Coroutine _spawnCoroutine;

    IEnumerator StartSpawn()
    {
        //  if (_starting)
        //      SpeedSpawnChange();
        //
        //  var message = PullObjects.GetMessage(CheckDirection());
        //
        //  Vector2 size = message.GetComponent<GenerateMessage>().Generate(_chanceBadMessages);
        //
        //  _wightInstance = size.x;
        //  _hightInstance = size.y;
        //
        //  message.SetActive(false);
        //
        //  Vector2 startPos = _messagesParent.position;
        //  Vector2 endPos = startPos + Vector2.up * (_hightInstance + _stepMessanges);
        //
        //  float t = 0;
        //
        //  WaitForEndOfFrame frame = new WaitForEndOfFrame();
        //
        //  while (t < 1)
        //  {
        //      Vector2 lerped = Vector2.Lerp(startPos, endPos, _curve.Evaluate(t));
        //      _messagesParent.position = lerped;
        //      t += _speedOfLevel * Time.deltaTime;
        //
        //      yield return frame;
        //  }
        //
        //  SetPosition(_currentDir, message);
        //  _spawnCoroutine = null;
        //
        //  SpawnMessages(); //recycle
        yield return null;
    }

    private void OnEnable()
    {
        //ScoreController.OnChangeLevelSpeed.AddListener(ChangeLevelSpeed);
    }
    private void OnDisable()
    {
        //ScoreController.OnChangeLevelSpeed.RemoveListener(ChangeLevelSpeed);
    }

    private void Start()
    {
        _startLevelSpeed = _speedOfLevel;

        _wightBackground = Camera.main.orthographicSize * 2 * Camera.main.aspect;
        _hightBackground = Camera.main.orthographicSize * 2f;

        SpawnMessages();
    }

    public void SpawnMessages()
    {
        int random = Random.Range(0, 2);

        switch (random)
        {
            case 0:
                _currentDir = EDirection.Right;
                break;
            case 1:
                _currentDir = EDirection.Left;
                break;
        }

        if (_spawnCoroutine == null)
            _spawnCoroutine = StartCoroutine(StartSpawn());
    }
    public void StopSpawn()
    {
        StopCoroutine(_spawnCoroutine);
        _spawnCoroutine = null;
    }

    public void ChanceBadMessages(int badChance)
    {
        _chanceBadMessages = badChance;
    }

    private void SpeedSpawnChange()
    {
       // _starting = (PullObjects.GetMessagesCount < _startCount) ? true : false;
        //_speedOfLevel = (PullObjects.GetMessagesCount < _startCount) ? 3 : _startLevelSpeed;
    }
    private void SetPosition(EDirection direction, GameObject message)
    {
        Vector2 position = Vector2.zero;
        Vector2 currentPos = Vector2.down * _hightBackground / 2;

        switch (direction)
        {
            case EDirection.Right:
                position = currentPos + new Vector2((-_wightBackground / 2 + _stepEdge) + _wightInstance / 2, _hightInstance / 2);
                break;
            case EDirection.Left:
                position = currentPos + new Vector2((_wightBackground / 2 - _stepEdge) - _wightInstance / 2, _hightInstance / 2);
                break;
        }

        message.transform.position = position;
        message.transform.rotation = Quaternion.identity;
        message.transform.parent = _messagesParent;
        message.SetActive(true);
    }
    private void ChangeLevelSpeed(float plusSpeed)
    {
        _speedOfLevel += plusSpeed;
    }
    private EDirection CheckDirection()
    {
        bool rightCheck = (_currentDir == EDirection.Right && _prevDir == EDirection.Right);
        bool leftCheck = (_currentDir == EDirection.Left && _prevDir == EDirection.Left);

        if (rightCheck) _countOfBlocks++;
        else if (leftCheck) _countOfBlocks++;
        else _countOfBlocks = 0;

        //_countOfBlocks = (rightCheck || leftCheck) ? _countOfBlocks++ : 0;

        if (_countOfBlocks >= _countOfSkip)
            _currentDir = (_currentDir == EDirection.Right) ? EDirection.Left : EDirection.Right;

        _prevDir = _currentDir;

        return _currentDir;
    }
}
