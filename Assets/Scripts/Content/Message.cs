﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class Message : AContent , IBorder
{
    [SerializeField] private Sprite _goodSprite;
    [SerializeField] private Sprite _badSprite;

    private TextMeshProUGUI _textMesh;
    private GameDataSO.MessageSetup _setup;

    private void Awake()
    {
        base.Awake();
        _textMesh = _rectTransformContent.GetComponent<TextMeshProUGUI>();
        _setup = _gameData.messageSetup;
    }

    protected override void SizeBeforeAssignContent()
    {
        _size = new Vector2();
        _size.x = Random.Range(_gameData.messageSetup.minWidth, _gameData.messageSetup.maxWidth);
        _size.y = 1;
        _rectTransformPlatform.sizeDelta = _size;
    }

    protected override EStyleContent RandomStyleContent()
    {
        EStyleContent style = EStyleContent.Normal;
        int random = Random.Range(0, 100);
        
        if (random <= _gameData.contentSetup.chanceForOpponent)
        {
            style = EStyleContent.ForOpponent;
            return style;
        }
        
        if (random <= _gameData.contentSetup.chanceUnique)
        {
            style = EStyleContent.Unique;
            return style;
        }

        return style;
    }

    protected override void Content()
    {
        string text;

        switch (_style)
        {
            case EStyleContent.Start:
            case EStyleContent.Unique:
                text = _curPerson.GetRandomReplicaPerson(_curSent);
                break;
            case EStyleContent.Normal:
                text = _gameData.messageSetup.textData.GetRandomReplica(_curSent);
                break;
            case EStyleContent.ForOpponent:
                var opponent = CoreSystem.opponentController.GetOpponent(_curDirection);
                text = opponent.GetRandomReplicaOpponent(_curSent);
                break;
            default:
                text = _gameData.messageSetup.textData.GetRandomReplica(_curSent);
                break;
        }
        
        InsertSmiles(ref text);
        
        _textMesh.text = text;
        _textMesh.ForceMeshUpdate();
    }
    
    protected override void SizeAfterAssignContent()
    {
        _size.y = _textMesh.textBounds.size.y + 0.2f;
        _rectTransformPlatform.sizeDelta = _size;
        _contentBack.size = _size;
    }

    private void InsertSmiles(ref string text)
    {
        int index = 0;

        while (index < text.Length)
        {
            switch (text[index])
            {
                case ' ':
                    if (index < text.Length - 1 && text[index + 1] == '8')
                        break;

                    if (Random.Range(0, 100) > _setup.chanceSmile)
                        break;

                    Change(ref text, ref index);
                    continue;
                case '8':
                    Change(ref text, ref index, false);
                    continue;
            }

            index++;
        }
    }
    
    private void Change(ref string text, ref int index, bool space = true)
    {
        text = text.Remove(index, 1);
        text = text.Insert(index, SmileCode(out var length, space));
        index += length;
    }
    
    private string SmileCode(out int length, bool space = true)
    {
        string smileCode;
        if (space)
        {
            smileCode = _curSent == ESent.Good
                ? $" <sprite={Random.Range(_setup.minIndexGoodSmile, _setup.maxIndexGoodSmile + 1)}> "
                : $" <sprite={Random.Range(_setup.minIndexBadSmile, _setup.maxIndexBadSmile + 1)}> ";
        }
        else
        {
            smileCode = _curSent == ESent.Good
                ? $"<sprite={Random.Range(_setup.minIndexGoodSmile, _setup.maxIndexGoodSmile + 1)}>"
                : $"<sprite={Random.Range(_setup.minIndexBadSmile, _setup.maxIndexBadSmile + 1)}>";
        }

        length = smileCode.Length;
        return smileCode;
    }

    public override void Generate(EStyleContent? style = null)
    {
        base.Generate(style);
        SetBorder();
    }

    public void SetBorder()
    {
        if (!_contentBack) return;

        var borderSprite = _goodSprite;

        switch (_curSent)
        {
            case ESent.Good:
                borderSprite = _goodSprite;
                break;
            case ESent.Bad:
                borderSprite = _badSprite;
                break;
            default:
                break;
        }

        _contentBack.sprite = borderSprite;
    }
}
