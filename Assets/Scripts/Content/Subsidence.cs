﻿using UnityEngine;
using DG.Tweening;

public class Subsidence : MonoBehaviour
{
    [SerializeField] private Color _goodFon;
    [SerializeField] private Color _badFon;

    private SpriteRenderer _sp;
    private AContent _content;

    private const float _delay = 0.06f;
    private const float _shift = 0.2f;
    private float _curDelay;

    private void Awake()
    {
        _sp = GetComponent<SpriteRenderer>();
        _content = GetComponent<AContent>();
    }

    private void OnEnable()
    {
        _curDelay = 0;
    }

    private void Update()
    {
        _curDelay += Time.deltaTime;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (_curDelay < _delay)
            return;

        var color = (_content.sent == ESent.Bad) ? _badFon : _goodFon;

        var localPos = transform.localPosition;

        Sequence seq = DOTween.Sequence();
        seq.Append(transform.DOLocalMoveY(localPos.y - _shift, _delay * 0.5f));
        seq.Append(transform.DOLocalMoveY(localPos.y, _delay * 0.5f));
        seq.Append(_sp.DOColor(color, _delay * 2));
        seq.Append(_sp.DOColor(Color.white, _delay * 2));
        seq.Play();

        _curDelay = 0;
    }
}
