﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


[CreateAssetMenu(fileName = "StickersData", menuName = "Dataset/StickersData")]
public class StickersDataSO : ScriptableObject
{
    [SerializeField] private Sprite[] _goodStickers;
    [SerializeField] private Sprite[] _badStickers;

    public Sprite GetRandomSticker(ESent sent)
    {
        var stickers = sent == ESent.Good ? _goodStickers : _badStickers;
        return stickers[Random.Range(0, stickers.Length)];
    }
}