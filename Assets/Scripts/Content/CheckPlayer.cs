﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class CheckPlayer : MonoBehaviour
{
    private PlayerController _player;
    private Collider2D _collider;

    private void Awake()
    {
        _collider = GetComponent<Collider2D>();
        _player = CoreSystem.gameData.reference.player;
    }

    private void OnEnable()
    {
        _collider.enabled = true;
    }

    private void Update()
    {
        if (!_collider.enabled)
            return;

        _player = CoreSystem.gameData.reference.player;
        
        if (!_player)
            return;
        
        if (_collider.bounds.min.y > _player.transform.position.y)
            _collider.enabled = false;
    }
}
