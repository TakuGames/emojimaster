﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBorder
{
    void SetBorder();
}

public abstract class AContent : MonoBehaviour
{
    private BoxCollider2D _collider;
    private ContentPull _contentPull;

    protected EDirection _curDirection;
    protected RectTransform _rectTransformPlatform;
    protected RectTransform _rectTransformContent;
    protected SpriteRenderer _contentBack;
    protected GameDataSO _gameData;
    protected PersonSO _curPerson;
    protected ESent _curSent;
    protected EStyleContent _style;
    protected Vector2 _size;

    public ContentPull contentPull
    {
        set => _contentPull = value;
    }

    public EDirection direction => _curDirection;
    public Vector2 size => _size;
    public ESent sent => _curSent;
    public PersonSO person => _curPerson;


    protected void Awake()
    {
        _contentBack = GetComponent<SpriteRenderer>();
        _collider = GetComponent<BoxCollider2D>();
        _rectTransformPlatform = GetComponent<RectTransform>();
        _rectTransformContent = transform.GetChild(0).GetComponent<RectTransform>();
        _gameData = CoreSystem.gameData;
    }

    public virtual void Generate(EStyleContent? style = null)
    {
        transform.position = new Vector2(0, -20);
        gameObject.SetActive(true);

        _style = style ?? RandomStyleContent();

        _curDirection = Direction();
        _curPerson = CoreSystem.opponentController.GetPerson(_curDirection);
        
        Sentiment();
        SizeBeforeAssignContent();
        Content();
        SizeAfterAssignContent();
        Collider();
        Rotation();
    }

    protected abstract EStyleContent RandomStyleContent();

    protected abstract void Content();


    protected abstract void SizeBeforeAssignContent();

    protected abstract void SizeAfterAssignContent();

    private void Sentiment()
    {
        _curSent = (ESent)Random.Range(0, 2);
        
        if (_curPerson.lastSent == _curSent)
        {
            _curSent = (ESent)Random.Range(0, 2);
        }

        _curPerson.lastSent = _curSent;
    }

    private void Collider()
    {
        _collider.size = new Vector2(_size.x, 0.5f);
        _collider.offset = new Vector2(0, -((_size.y * 0.5f) + 0.2f));
    }

    private void Rotation()
    {
        var newRotation = _curDirection == EDirection.Right ? new Vector3(0, 0, 0) : new Vector3(0, 180, 0);
        _rectTransformPlatform.rotation = Quaternion.Euler(newRotation);
        _rectTransformContent.localRotation = Quaternion.Euler(newRotation);
    }

    private EDirection Direction()
    {
        EDirection direction;
        EDirection lastDirection = CoreSystem.levelController.lastDirection;

        if (_style == EStyleContent.Start)
        {
            //direction = lastDirection == EDirection.Left ? EDirection.Right : EDirection.Left;
            direction = EDirection.Left;
        }
        else
        {
            direction = (EDirection)Random.Range(0, 2);

            if (direction == lastDirection)
                direction = (EDirection)Random.Range(0, 2);
        }

        CoreSystem.levelController.lastDirection = direction;
        return direction;
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
        _contentPull.ReturnMessage(this);
    }
}