﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using Random = UnityEngine.Random;


[CreateAssetMenu(fileName = "TextData", menuName = "Dataset/TextData")]
public class TextDataSO : ScriptableObject
{
    [SerializeField] private TextAsset _goodTextAsset;
    [SerializeField] private TextAsset _badTextAsset;

    private string[] _goodLines;
    private string[] _badLines;

    private void Awake()
    {
        Validate();
    }

    public string GetRandomReplica(ESent sent)
    {
        var text = sent == ESent.Good ? _goodLines : _badLines;
        return text[Random.Range(0, text.Length)];
    }

    private void OnValidate()
    {
        Validate();
    }

    private void Validate()
    {
        if (_goodTextAsset != null)
            _goodLines = _goodTextAsset.text.Split(new[] {Environment.NewLine}, StringSplitOptions.None);

        if (_badTextAsset != null)
            _badLines = _badTextAsset.text.Split(new[] {Environment.NewLine}, StringSplitOptions.None);
    }
}