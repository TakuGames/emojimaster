﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Sticker : AContent , IBorder
{
    [SerializeField] private Sprite _goodSprite;
    [SerializeField] private Sprite _badSprite;

    private Image _image;
    private byte _sizeRect;

    private byte sizeRect
    {
        get
        {
            if (_sizeRect == 0)
                _sizeRect = _gameData.stickerSetup.sizeRect;

            return _sizeRect;
        }
    }

    private void Awake()
    {
        base.Awake();
        _image = _rectTransformContent.GetComponent<Image>();
    }

    protected override void SizeBeforeAssignContent()
    {
        _size = new Vector2(sizeRect, sizeRect);
        _rectTransformPlatform.sizeDelta = _size;
    }

    protected override EStyleContent RandomStyleContent()
    {
        EStyleContent style = EStyleContent.Normal;
        int random = Random.Range(0, 100);

        if (random <= _gameData.contentSetup.chanceUnique)
        {
            style = EStyleContent.Unique;
            return style;
        }

        return style;
    }

    protected override void Content()
    {
        switch (_style)
        {
            case EStyleContent.Unique:
                _image.sprite = _curPerson.GetRandomSticker(_curSent);
                break;
            default:
                _image.sprite = _gameData.stickerSetup.stickersData.GetRandomSticker(_curSent);
                break;
        }
    }

    protected override void SizeAfterAssignContent()
    {
        _contentBack.size = _size;
    }

    public override void Generate(EStyleContent? style = null)
    {
        base.Generate(style);
        SetBorder();
    }

    public void SetBorder()
    {
        if (!_contentBack) return;

        var borderSprite = _goodSprite;

        switch (_curSent)
        {
            case ESent.Good:
                borderSprite = _goodSprite;
                break;
            case ESent.Bad:
                borderSprite = _badSprite;
                break;
            default:
                break;
        }

        _contentBack.sprite = borderSprite;
    }
}