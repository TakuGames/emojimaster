﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContentPull
{
    private GameObject _contentPrefab;
    private Stack<AContent> _contents;

    public ContentPull(string contentType)
    {
        _contentPrefab = Resources.Load(contentType) as GameObject;
        _contents = new Stack<AContent>();
    }

    public AContent GetContent()
    {
        AContent content = _contents.Count == 0 ? Object.Instantiate(_contentPrefab).GetComponent<AContent>() : _contents.Pop();
        
        content.contentPull = this;
        return content;
    }

    public void ReturnMessage(AContent content)
    {
        _contents.Push(content);
    }
}
