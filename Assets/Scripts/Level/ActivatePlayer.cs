﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivatePlayer : MonoBehaviour
{
    [SerializeField] private GameObject _playerPrefab;
    [SerializeField] private int _countFirstContent = 2;
    private void Update()
    {
        if (CoreSystem.levelController.contentCount >= _countFirstContent)
        {
            Instantiate(_playerPrefab);
            Destroy(this);
        }
    }
}
