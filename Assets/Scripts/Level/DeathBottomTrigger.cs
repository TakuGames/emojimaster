﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathBottomTrigger : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        other.GetComponent<IDie>().Die(true);
    }
}
