﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class DeathTopTrigger : MonoBehaviour
{
    private Collider2D _collider;
    private RaycastHit2D _hit;
    private int _mask;
    private Vector2 _origin;
    private float? _playerScale;
    
    private float _getPlayerScale
    {
        get
        {
            if (_playerScale == null)
            {
                var player = FindObjectOfType<PlayerController>();
                _playerScale = player.transform.localScale.y;
            }

            return (float) _playerScale;
        }
    }

    private void Awake()
    {
        _collider = GetComponent<Collider2D>();
    }
    private void Start()
    {
        _mask = 1 << LayerMask.NameToLayer("Platform");
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        var player = collision.gameObject.GetComponent<AdderLikes>();
        if (!player) return;

        player.SetAddingLikes(false);
    }
    private void OnCollisionStay2D(Collision2D other)
    {
        CheckSizePlayer(other.collider);
        CheckDeath(other.collider);
    }
    private void OnCollisionExit2D(Collision2D other)
    {
        CheckSizePlayer(other.collider);

        var player = other.gameObject.GetComponent<AdderLikes>();
        if (!player) return;

        player.SetAddingLikes(true);
    }

    private void CheckSizePlayer(Collider2D other)
    {
        _origin = new Vector2(other.bounds.center.x, _collider.bounds.min.y);
        _hit = Physics2D.Raycast(_origin, Vector2.down, 2f, _mask);

        if (!_hit.collider)
        {
            if (other)
                other.transform.localScale = new Vector3(_getPlayerScale, _getPlayerScale, 1f);
        }
        else
        {
            Debug.DrawLine(_origin, _hit.point, Color.black);

            var newScale = other.transform.localScale;
            newScale.y = Mathf.Clamp(_hit.distance, 0, _getPlayerScale);
            other.transform.localScale = newScale;
        }   
    }
    private void CheckDeath(Collider2D other)
    {
        if (other.transform.localScale.y < 0.3f)
            other.GetComponent<IDie>().Die(true);
    }
}
