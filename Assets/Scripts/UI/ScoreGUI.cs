﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreGUI : MonoBehaviour
{
    private static readonly int _playId = Animator.StringToHash("Play");
    
    private TextMeshProUGUI _valueTxt;
    private Animator _animator;
    

    private void Awake()
    {
        _valueTxt = GetComponent<TextMeshProUGUI>();
        _animator = GetComponent<Animator>();
    }

    public void Show(int value)
    {
        _valueTxt.text = value.ToString();
        _animator.SetTrigger(_playId);
    }
}