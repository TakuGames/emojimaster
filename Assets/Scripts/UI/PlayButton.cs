﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayButton : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _playVsTxt;
    [SerializeField] private int _nextSceneIndex;

    private AsyncOperation _asyncLoader;

    private void OnEnable()
    {
        _asyncLoader = SceneManager.LoadSceneAsync(_nextSceneIndex);
        _asyncLoader.allowSceneActivation = false;

        if (_playVsTxt)
            _playVsTxt.text = $"PLAY";
    }

    public void LoadScene(int index)
    {
        _asyncLoader.allowSceneActivation = true;
    }
}
