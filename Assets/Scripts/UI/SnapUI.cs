﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class SnapUI : MonoBehaviour
{
    public int direction => _direction;

    [SerializeField] private RectTransform _scrollPanel;
    [SerializeField] private ScrollRect _rectPanel;

    [SerializeField] private float _sensitivity;
    [SerializeField] private float _snapSpeed;

    [SerializeField] private bool _horSnap;

    private bool _draggin;

    private int _direction;

    private float _step;
    private float _prevPos;
    private float _destination;
    private float _snappingDistance;

    private void Start()
    {
        Initialization();
    }
    private void LateUpdate()
    {
        Check();
        Rolling();
    }

    /// <summary>
    /// Only for square
    /// </summary>
    /// <returns></returns>
    private float GetStep()
    {
        Vector3[] vec = new Vector3[4];
        _scrollPanel.GetWorldCorners(vec);

        float step = (_scrollPanel.rect.width > _scrollPanel.rect.height) ? 
            Mathf.Abs(vec[1].y - vec[0].y) :
            Mathf.Abs(vec[2].x - vec[0].x);

        return step;
    }
    private void Check()
    {
        float scrollPos = (_horSnap) ?
            _scrollPanel.transform.position.x :
            _scrollPanel.transform.position.y;

        if (Math.Abs(scrollPos - _prevPos) >= _step - _snappingDistance)
        {
            if (direction == 0)
                return;

            _prevPos += direction * _step;
            _destination = _prevPos;
        }
    }
    private void Rolling()
    {
        float velocity = (_horSnap) ?
            _rectPanel.velocity.x :
            _rectPanel.velocity.y;

        if (_draggin)
        {
            _direction = (velocity > 0) ? 1 : -1;
            return;
        }

        if (Mathf.Abs(velocity) > _sensitivity)
            return;

        var currentScrollPos = _scrollPanel.position;

        if (_horSnap)
            currentScrollPos.x = Mathf.MoveTowards(_scrollPanel.position.x, _destination, Time.deltaTime * _snapSpeed);
        else
            currentScrollPos.y = Mathf.MoveTowards(_scrollPanel.position.y, _destination, Time.deltaTime * _snapSpeed);

        _scrollPanel.position = currentScrollPos;
    }

    public void Initialization()
    {
        _step = GetStep();
        _snappingDistance = _step * 0.5f;

    }
    public void ChangeScrollPos(float currentPos)
    {
        float finalPos = 0;

        finalPos = -currentPos;
        var pos = _scrollPanel.position;

        if (_horSnap) pos.x = finalPos;
        else pos.y = finalPos;

        _prevPos = finalPos;
        _destination = finalPos;
        _scrollPanel.position = pos;
    }

    public void Draggin(bool value)
    {
        _draggin = value;
    }
}

