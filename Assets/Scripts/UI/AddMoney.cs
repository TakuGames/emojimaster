﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddMoney : MonoBehaviour
{
    private Money _money;

    private void Awake()
    {
        _money = Money.instance;
    }

    public void Add(int value)
    {
        _money.Add(value);
    }
}
