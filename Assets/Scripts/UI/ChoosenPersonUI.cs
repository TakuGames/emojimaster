﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChoosenPersonUI : MonoBehaviour
{
    [SerializeField] private bool _leftScroll;
    [SerializeField] private RectTransform _scrollPanel;

 //   private StateGame _stateGame;
    private PersonUI[] _persons;

    private float _snappingDistance;

    private void OnEnable()
    {
 //       _stateGame = StateGame.Instance();

        _persons = _scrollPanel.GetComponentsInChildren<PersonUI>();
        _snappingDistance = GetStep() * 0.2f;
    }
//    private void Update()
//    {
//        CheckChoosenPerson();
//    }
//
//    private void CheckChoosenPerson()
//    {
//        for (int i = 0; i < _opponents.Length; i++)
//        {
//            if (Mathf.Abs(_opponents[i].transform.position.y - _scrollPanel.parent.transform.position.y) <= _snappingDistance)
//            {
//                // некрасивая бяка
//                if (_leftScroll)
//                    _stateGame.player = _opponents[i].person;
//                else
//                    _stateGame.opponent = _opponents[i].person;
//
//                if (!_opponents[i].chosen)
//                    _opponents[i].chosen = true;
//            }
//            else
//            {
//                if (_opponents[i].chosen)
//                    _opponents[i].chosen = false;
//            }
//        }
//    }
    private float GetStep()
    {
        Vector3[] vec = new Vector3[4];
        _scrollPanel.GetWorldCorners(vec);

        float step = Mathf.Abs(vec[2].x - vec[0].x);
        return step;
    }
}
