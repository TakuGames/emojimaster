﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectPlayerBtn : ASelectPlayerBtn
{
    private SelectPlayerController _controller;

    private void Awake()
    {
        _controller = GetComponentInParent<SelectPlayerController>();
    }

    public void OnBtn()
    {
        StateGame.SetPlayer(_controller.selectedPerson);
    }
}
