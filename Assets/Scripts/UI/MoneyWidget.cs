﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MoneyWidget : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _moneyValueTxt;

    private Money _money;

    private void Start()
    {
        _money = Money.instance;
        Refresh();
        _money.onChangeMoney.AddListener(Refresh);
    }

    private void Refresh()
    {
        _moneyValueTxt.text = _money.getValue.ToString();
    }

    private void OnDestroy()
    {
        _money.onChangeMoney.RemoveListener(Refresh);
    }
}
