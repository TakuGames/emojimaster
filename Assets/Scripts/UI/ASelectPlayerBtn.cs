﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ASelectPlayerBtn : MonoBehaviour
{
    protected PersonSO _personData;
    
    public virtual void Show()
    {
        gameObject.SetActive(true);
    }
    
    public virtual void Show(PersonSO personData)
    {
        gameObject.SetActive(true);
        _personData = personData;
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
