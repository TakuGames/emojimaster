﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardBtnsUI : MonoBehaviour
{
    [SerializeField] private GameObject _rewardX2Btn;
    [SerializeField] private GameObject _rewardBtn;
    [SerializeField] private float _delaySecondShowRewardBtn = 2f;

    private void Start()
    {
        _rewardX2Btn.SetActive(false);
        _rewardBtn.SetActive(false);
    }

    public void Show()
    {
        StartCoroutine(ShowRewardBtn());
    }

    private IEnumerator ShowRewardBtn()
    {
        _rewardX2Btn.SetActive(true);
        yield return new WaitForSeconds(_delaySecondShowRewardBtn);
        _rewardBtn.SetActive(true);
        
        Debug.Log(Reward.reward);
        Debug.Log(Reward.result);
    }

    //Called in UI
    public void OnBtnRewardX2()
    {
        Money.instance.Add(Reward.reward * 2);
        LoadScene.LoadStartScene();
    }
    
    //Called in UI
    public void OnBtnReward()
    {
        Money.instance.Add(Reward.reward);
        LoadScene.LoadStartScene();
    }
}
