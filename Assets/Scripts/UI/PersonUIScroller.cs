﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class PersonUIScroller : PersonUI
{
    [SerializeField] private Mask _mask;
    [SerializeField] private GameObject _StartLikes;
    [SerializeField] private TextMeshProUGUI _startLikesCountTxt;

    private Color _currentColor;

    protected override void AssignAvatar()
    {
        base.AssignAvatar();
        _startLikesCountTxt.text = _person.power.startLikes.ToString();
    }

    public void ColorazePerson(Color color)
    {
        if (_currentColor == color)
            return;

        if (_currentColor != color)
            _currentColor = color;

        for (int i = 0; i < _personsParts.parts.Length; i++)
        {
            Material mat = Instantiate(_personsParts.parts[i].material);
            mat.SetColor("_Color", color);
            _personsParts.parts[i].material = mat;

            _mask.OnSiblingGraphicEnabledDisabled();
        }
    }
    public void ColorazePerson(LeagueSO league)
    {
        if (_currentColor == league.color)
            return;

        if (_currentColor != league.color)
            _currentColor = league.color;

        _personsParts.circle.color = league.color;

        Material mat = Instantiate(_personsParts.circle.material);
        mat.SetColor("_Color", league.color);
        _personsParts.circle.material = mat;
        
        _StartLikes.SetActive(true);

        _mask.OnSiblingGraphicEnabledDisabled();

    }
    public void ColorazePerson(Color color, int startIndex)
    {
        if (_currentColor == color)
            return;

        if (_currentColor != color)
            _currentColor = color;
        
        _StartLikes.SetActive(false);

        for (int i = startIndex; i < _personsParts.parts.Length; i++)
        {
            Material mat = Instantiate(_personsParts.parts[i].material);
            mat.SetColor("_Color", color);
            _personsParts.parts[i].material = mat;

            _mask.OnSiblingGraphicEnabledDisabled();
        }
    }
}
