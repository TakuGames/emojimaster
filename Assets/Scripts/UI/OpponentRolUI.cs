﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonRolUi : PersonUI
{
    [SerializeField] private GameObject _block;

    public void CheckBlock()
    {
        _block.SetActive(_person.defeated);
        Debug.Log("Rol");
    }
}
