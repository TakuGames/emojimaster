﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerUI : MonoBehaviour
{
    [Header("[Tower Property]")]
    [SerializeField] private GameObject _leagueInfoElement;
    [SerializeField] private Transform _content;

    private List<GameObject> _elementPool = new List<GameObject>();

    private void OnEnable()
    {
        UpdatePlayers();
    }

    private void AssignCountChilds(int countPersons)
    {
        if (_elementPool.Count < countPersons)
        {
            var countAdded = countPersons - _elementPool.Count;

            for (int i = 0; i < countAdded; i++)
            {
                var player = Instantiate(_leagueInfoElement, _content);
                _elementPool.Add(player);
            }
        }
        else
        {
            for (int i = 0; i < _elementPool.Count; i++)
            {
                _elementPool[i].SetActive(false);
            }
            for (int i = 0; i < countPersons; i++)
            {
                _elementPool[i].SetActive(true);
            }
        }
    }
    private void AssignRectTransform(int countPersons, bool reverse = false)
    {
        var rectTransformContent = _content.GetComponent<RectTransform>();

        var startPos = rectTransformContent.position;

        bool checkSize = rectTransformContent.rect.height < rectTransformContent.rect.width;

        if (checkSize)
            startPos.x = 0;
        else
            startPos.y = 0;

        rectTransformContent.position = startPos;

        var rectSample = _leagueInfoElement.GetComponent<RectTransform>().rect;
        var layoutGroup = _content.GetComponent<HorizontalOrVerticalLayoutGroup>();

        Vector2 currentSize = rectTransformContent.sizeDelta;

        if (checkSize)
            currentSize.x = (countPersons) * (rectSample.width + layoutGroup.spacing);
        else
            currentSize.y = (countPersons) * (rectSample.height + layoutGroup.spacing);

        rectTransformContent.sizeDelta = currentSize;

        for (int i = 0; i < countPersons; i++)
        {
            if (reverse)
                _elementPool[i].GetComponent<RectTransform>().SetAsFirstSibling();
            else
                _elementPool[i].GetComponent<RectTransform>().SetAsLastSibling();
        }

        layoutGroup.enabled = true;

        LayoutRebuilder.ForceRebuildLayoutImmediate(_content.GetComponent<RectTransform>());

        layoutGroup.enabled = false;
    }
    private void AssignPersonsInfo(LeagueSO currentLeague)
    {
        for (int i = 0; i < currentLeague.personsCount; i++)
        {
            var currentPerson = _elementPool[i].GetComponentInChildren<PersonUI>();
            currentPerson.person = currentLeague.GetPerson(i);

            _elementPool[i].name = currentPerson.person.name;
            _elementPool[i].SetActive(true);
        }
    }

    public void UpdatePlayers()
    {
        var currentLeague = StateGame.opponent.league;

        if (!currentLeague) return;

        AssignCountChilds(currentLeague.personsCount);
        AssignRectTransform(currentLeague.personsCount, true);
        AssignPersonsInfo(currentLeague);

        _content.position = Vector2.up * _content.GetComponent<RectTransform>().sizeDelta / 2;
    }
}
