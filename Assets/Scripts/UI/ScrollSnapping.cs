﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollSnapping : MonoBehaviour
{
    [SerializeField] private float _snapSpeed;

    private List<RectTransform> _elementsOfContent = new List<RectTransform>();
    private List<RectTransform> _invisiableElements = new List<RectTransform>();

    private RectTransform _contentRectTransform;

    private bool _isScrolling;
    private int _elementId;

    private Vector2 _desiredVector;

    IEnumerator StartSnapping()
    {
        WaitForEndOfFrame frame = new WaitForEndOfFrame();

        yield return frame;

        while (true)
        {
            for (int i = 0; i < _elementsOfContent.Count; i++)
            {
                var dist = Mathf.Abs(_contentRectTransform.localPosition.y - _elementsOfContent[i].localPosition.y);

                if (dist <= _elementsOfContent[i].rect.height / 2)
                {
                    if (i != _elementId)
                        _elementId = i;
                }
            }

            if (!_isScrolling)
            {
                _desiredVector.x = _contentRectTransform.localPosition.x;
                _desiredVector.y = Mathf.Lerp(_contentRectTransform.localPosition.y, _elementsOfContent[_elementId].localPosition.y, _snapSpeed);
                _contentRectTransform.localPosition = _desiredVector;
            }

            yield return frame;
        }
    }

    private void Awake()
    {
        var elements = GetComponentsInChildren<Button>();

        for (int i = 0; i < elements.Length; i++)
        {
            var elementRect = elements[i].GetComponent<RectTransform>();
            _elementsOfContent.Add(elementRect);
        }

        _contentRectTransform = GetComponent<RectTransform>();
    }
    private void OnEnable()
    {
        StartCoroutine(StartSnapping());
    }

    public void Scrolling(bool scrollValue)
    {
        _isScrolling = scrollValue;
    }
    public void Scrolling()
    {
        Debug.Log("Scroll");
    }
}
