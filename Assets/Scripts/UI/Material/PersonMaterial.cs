﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PersonMaterial : MonoBehaviour  
{
    [Serializable]
    public struct ColorSettings
    {
        public Color selectedColor;
        public Color unselectedColor;
        public Color noneAvailableColor;
    }
    [Serializable]
    public struct References
    {
        public Mask personMask;
        public Image[] personsParts;
    }

    [SerializeField] private References _references;
    [SerializeField] private ColorSettings _colorSettings;

    private Color _currentColor;

    public void SelectPerson(bool value)
    {
        var color = (value) ? _colorSettings.selectedColor : _colorSettings.unselectedColor;

        if (_currentColor == color)
            return;

        if (_currentColor != color)
            _currentColor = color;

        for (int i = 0; i < _references.personsParts.Length; i++)
        {
            Material mat = Instantiate(_references.personsParts[i].material);
            mat.SetColor("_Color", color);
            _references.personsParts[i].material = mat;

            _references.personMask.OnSiblingGraphicEnabledDisabled();
        }    
    }
}
