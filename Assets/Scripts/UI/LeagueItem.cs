﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LeagueItem : MonoBehaviour
{
    [SerializeField] private Image _leagueLogo;

    private Color _currentColor;

    protected LeagueSO _league;

    public LeagueSO league
    {
        get => _league;
        set
        {
            _league = value;
            Assign();
        }
    }

    private void Assign()
    {
        _leagueLogo.sprite = _league.logo;
    }

    public void SelectLeague(Color color)
    {
        if (_currentColor == color)
            return;

        if (_currentColor != color)
            _currentColor = color;

        RecolorImage(_leagueLogo, color);
    }
    private void RecolorImage(Image image, Color color)
    {
        Material mat = Instantiate(image.material);
        mat.SetColor("_Color", color);
        image.material = mat;
    }
}
