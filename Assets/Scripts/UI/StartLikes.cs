﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StartLikes : MonoBehaviour
{
    [SerializeField] private GameObject _addLikeBtn;
    [SerializeField] private TextMeshProUGUI _likesCountTxt;
    private PersonSO _person;
    private Money _money;

    private Money _getMoney
    {
        get
        {
            if (_money == null)
                _money = Money.instance;

            return _money;
        }
    }

    private void Start()
    {
        _getMoney.onChangeMoney.AddListener(Refresh);
    }

    public void Assign(PersonSO person)
    {
        _person = person;
        Refresh();
    }

    private void Refresh()
    {
        _addLikeBtn.SetActive(IsShowAddBtn());
        _likesCountTxt.text = (_person.power.startLikes + _person.power.additionsStartLikes).ToString();
    }

    private bool IsShowAddBtn()
    {
        if (!_person.price.acquired)
            return false;

        if (!_getMoney.IncomeCheck(50))
            return false;

        if (MaxLikes() <= _person.power.startLikes + _person.power.additionsStartLikes)
            return false;

        return true;
    }

    public void AddLike()
    {
        _person.power.additionsStartLikes++;
        _getMoney.Subtract(50);
        Refresh();
    }

    private int MaxLikes()
    {
        var opponent = StateGame.opponent.league.GetPerson(0);
        return opponent.power.scoreTarget - 10;
    }
}
