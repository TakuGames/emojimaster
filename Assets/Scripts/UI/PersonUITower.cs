﻿using UnityEngine.UI;
using UnityEngine;
using System;
using TMPro;

public class PersonUITower : PersonUI
{
    [Serializable]
    public struct DescriptionPart
    {
        public GameObject serialNumber;
        public GameObject startLikes;
        public GameObject speedLikes;
        public GameObject targetLikes;
        public GameObject deafeated;
        public GameObject goldenFrame;


        private TextMeshProUGUI _targetTxt;
        private TextMeshProUGUI _startLikesTxt;
        private TextMeshProUGUI _speedLikesTxt;
        private TextMeshProUGUI _deafeated;
        private TextMeshProUGUI _serialNumber;

        private Image _goldenFrame;

        public void Awaking()
        {
            _targetTxt = targetLikes.GetComponentInChildren<TextMeshProUGUI>(true);
            _startLikesTxt = startLikes.GetComponentInChildren<TextMeshProUGUI>(true);
            _speedLikesTxt = speedLikes.GetComponentInChildren<TextMeshProUGUI>(true);
            _serialNumber = serialNumber.GetComponentInChildren<TextMeshProUGUI>(true);
            _deafeated = deafeated.GetComponentInChildren<TextMeshProUGUI>();
            _goldenFrame = goldenFrame.GetComponentInChildren<Image>();
        }
        public void UpdatingInfo(PersonSO person)
        {
            var power = person.power;

            startLikes.SetActive(false);
            speedLikes.SetActive(false);
            targetLikes.SetActive(false);

            if (person.defeated)
            {
                deafeated.SetActive(true);
                _deafeated.text = $"DEFEATED";
            }
            else
            {
                targetLikes.SetActive(true);
                _targetTxt.text = $"TARGET: {power.scoreTarget.ToString()}";

                if (power.startLikes > 0)
                {
                    startLikes.SetActive(true);
                    _startLikesTxt.text = $"Start likes: {power.startLikes.ToString()}";
                }
                if (power.additionsLikesInMinute > 0)
                {
                    speedLikes.SetActive(true);
                    _speedLikesTxt.text = $"{power.additionsLikesInMinute.ToString()} likes /min";
                }
            }

            var currentLeague = StateGame.opponent.league;
            var currentOpponentData = StateGame.opponent.personData;

            for (int i = 0; i < currentLeague.personsCount; i++)
            {
                if (person.name == currentLeague.persons[i].name)
                {
                    var serialNumber = currentLeague.personsCount - i;
                    _serialNumber.text = serialNumber.ToString();
                }
            }

            goldenFrame.SetActive(person.name == currentOpponentData.name);
        }
    }

    [SerializeField] private DescriptionPart _descriptionPart;

    protected override void AssignAvatar()
    {
        base.AssignAvatar();
        //_descriptionPart.Awaking();
        //_descriptionPart.UpdatingInfo(_person);
    }
}
