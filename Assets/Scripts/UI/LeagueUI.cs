﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LeagueUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _labelTxt;
    [SerializeField] private Image _background;
    
    protected LeagueSO _league;

    public LeagueSO league
    {
        get => _league;
        set
        {
            _league = value;
            Assign();
        }
    }

    private void Assign()
    {
        _labelTxt.text = _league.name;
        _background.sprite = _league.descriptionBack;
    }
}
