﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ChangeLeagueEvent : UnityEvent<LeagueSO> { }
public class ChangePersonEvent : UnityEvent<PersonSO> { }

public class ScrollersController : MonoBehaviour
{
    public ChangeLeagueEvent OnChangeLeague = new ChangeLeagueEvent();
    public ChangePersonEvent OnChangePerson = new ChangePersonEvent();

    [Header("[League Property]")]
    [SerializeField] private GameObject _leaguePrefab;
    [SerializeField] private Transform _spawnTransformLeague;

    [Header("[Player Property]")]
    [SerializeField] private GameObject _player;
    [SerializeField] private Transform _spawnTransformPlayers;

    private Dictionary<string, float> _startPosLeagues = new Dictionary<string, float>();
    private Dictionary<string, float> _startPosPlayers = new Dictionary<string, float>();

    private List<GameObject> _playersPool = new List<GameObject>();

    private void OnEnable()
    {
        if (_spawnTransformLeague)
        {
            var scalerLeaugue = _spawnTransformLeague.parent.GetComponent<ScalerCenter>();
            if (scalerLeaugue) scalerLeaugue.OnLeagueEvent.AddListener(UpdatePlayers);
        }
        if (_spawnTransformPlayers)
        {
            var scalerPerson = _spawnTransformPlayers.parent.GetComponent<ScalerCenter>();
            if (scalerPerson) scalerPerson.OnPersonEvent.AddListener(UpdatePlayers);
        }
    }
    private void Start()
    {
        CreateLeagues();
    }
    private void OnDisable()
    {
        if (_spawnTransformLeague)
        {
            var scalerLeaugue = _spawnTransformLeague.parent.GetComponent<ScalerCenter>();
            if (scalerLeaugue) scalerLeaugue.OnLeagueEvent.RemoveListener(UpdatePlayers);
        }
        if (_spawnTransformPlayers)
        {
            var scalerPerson = _spawnTransformPlayers.parent.GetComponent<ScalerCenter>();
            if (scalerPerson) scalerPerson.OnPersonEvent.RemoveListener(UpdatePlayers);
        }
    }

    private void CreateLeagues()
    {
        if (!_spawnTransformLeague)
            return;

        //create new full list of leagues
        List<LeagueSO> leagueSOs = new List<LeagueSO>();

        leagueSOs.Add(StateGame.data.single);

        for (int i = 0; i < StateGame.data.leaguesCount; i++)
        {
            leagueSOs.Add(StateGame.data.leagues[i]);
        }

        // get size and rectTransform of content
        var rectTransformContent = _spawnTransformLeague.GetComponent<RectTransform>();

        var startPos = rectTransformContent.position;
        startPos.x = 0;
        rectTransformContent.position = startPos;

        var rectSample = _leaguePrefab.GetComponent<RectTransform>().rect;

        // change size of rectTransform content
        Vector2 currentSize = rectTransformContent.sizeDelta;
        currentSize.x = (leagueSOs.Count) * rectSample.width;
        rectTransformContent.sizeDelta = currentSize;

        // create league gameObjects for start position
        List<GameObject> leagueList = new List<GameObject>();

        // spawn all leagues
        for (int i = 0; i < leagueSOs.Count; i++)
        {
            var sample = Instantiate(_leaguePrefab, _spawnTransformLeague);
            var league = sample.GetComponent<LeagueItem>();

            league.league = leagueSOs[i];
            leagueList.Add(sample);
            sample.name = league.league.name;
        }

        // set horizontal for leagues gameobjects
        var horizontalGroup = _spawnTransformLeague.GetComponent<HorizontalLayoutGroup>();
        horizontalGroup.enabled = true;

        LayoutRebuilder.ForceRebuildLayoutImmediate(_spawnTransformLeague.GetComponent<RectTransform>());

        // set start positions for league
        for (int i = 0; i < leagueList.Count; i++)
        {
            var league = leagueSOs[i];
            var key = league.name;
            _startPosLeagues.Add(key, leagueList[i].transform.position.x);
        }

        string currentLeagueName = "";

        if (StateGame.player != null)
            currentLeagueName = StateGame.player.league.name;

        if (!_startPosLeagues.ContainsKey(currentLeagueName))
            currentLeagueName = leagueSOs[0].name;

        float leaguePosX = _startPosLeagues[currentLeagueName];

        var snapperLeague = _spawnTransformLeague.parent.GetComponent<SnapUI>();
        snapperLeague.ChangeScrollPos(leaguePosX);
    }
    private void UpdatePlayers(LeagueSO currentLeague)
    {
        AssignCountChilds(currentLeague.personsCount);
        AssignRectTransform(currentLeague.personsCount);
        AssignPersonsInfo(currentLeague);
        InitializePersonsPos(currentLeague);

        OnChangeLeague.Invoke(currentLeague);
    }
    private void UpdatePlayers(PersonSO currentPerson, LeagueSO league)
    {
        OnChangePerson.Invoke(currentPerson);
    }

    private void AssignCountChilds(int countPersons)
    {
        if (_playersPool.Count < countPersons)
        {
            var countAdded = countPersons - _playersPool.Count;

            for (int i = 0; i < countAdded; i++)
            {
                var player = Instantiate(_player, _spawnTransformPlayers);
                _playersPool.Add(player);
            }
        }
        else
        {
            for (int i = 0; i < _playersPool.Count; i++)
            {
                _playersPool[i].SetActive(false);
            }
            for (int i = 0; i < countPersons; i++)
            {
                _playersPool[i].SetActive(true);
            }
        }
    }
    private void AssignRectTransform(int countPersons, bool reverse = false)
    {
        var rectTransformContent = _spawnTransformPlayers.GetComponent<RectTransform>();

        var startPos = rectTransformContent.position;

        bool checkSize = rectTransformContent.rect.height < rectTransformContent.rect.width;

        if (checkSize)
            startPos.x = 0;
        else
            startPos.y = 0;

        rectTransformContent.position = startPos;

        var rectSample = _player.GetComponent<RectTransform>().rect;
        var layoutGroup = _spawnTransformPlayers.GetComponent<HorizontalOrVerticalLayoutGroup>();

        Vector2 currentSize = rectTransformContent.sizeDelta;

        if (checkSize)
            currentSize.x = (countPersons) * (rectSample.width + layoutGroup.spacing);
        else
            currentSize.y = (countPersons) * (rectSample.height + layoutGroup.spacing);

        rectTransformContent.sizeDelta = currentSize;

        for (int i = 0; i < countPersons; i++)
        {
            if (reverse)
                _playersPool[i].GetComponent<RectTransform>().SetAsFirstSibling();
            else
                _playersPool[i].GetComponent<RectTransform>().SetAsLastSibling();
        }

        layoutGroup.enabled = true;

        LayoutRebuilder.ForceRebuildLayoutImmediate(_spawnTransformPlayers.GetComponent<RectTransform>());

        layoutGroup.enabled = false;
    }
    private void AssignPersonsInfo(LeagueSO currentLeague)
    {
        for (int i = 0; i < currentLeague.personsCount; i++)
        {
            var currentPerson = _playersPool[i].GetComponent<PersonUI>();
            currentPerson.person = currentLeague.GetPerson(i);

            _playersPool[i].name = currentPerson.person.name;
            _playersPool[i].SetActive(true);
        }
    }
    private void InitializePersonsPos(LeagueSO currentLeague)
    {
        _startPosPlayers.Clear();

        var rectTransformContent = _spawnTransformPlayers.GetComponent<RectTransform>();

        bool checkSize = rectTransformContent.rect.height < rectTransformContent.rect.width;

        for (int i = 0; i < currentLeague.personsCount; i++)
        {
            float startPos = (checkSize) ? _playersPool[i].transform.position.x : _playersPool[i].transform.position.y;

            _startPosPlayers.Add(_playersPool[i].name, startPos);
        }
        var playersSnapper = _spawnTransformPlayers.parent.GetComponent<SnapUI>();

        if (playersSnapper) playersSnapper.Initialization();

        string currentPlayerName = "";

        if (StateGame.player != null)
            currentPlayerName = StateGame.player.personData.name;

        if (!_startPosPlayers.ContainsKey(currentPlayerName))
            currentPlayerName = currentLeague.persons[0].name;

        float playerPosX = _startPosPlayers[currentPlayerName];

        if (playersSnapper) playersSnapper.ChangeScrollPos(playerPosX);

        var scaleCeneter = _spawnTransformPlayers.parent.GetComponent<ScalerCenter>();

        if (scaleCeneter) scaleCeneter.Initialization();
    }

    public void UpdatePlayers()
    {
        var currentLeague = StateGame.opponent.league;

        if (!currentLeague) return;

        AssignCountChilds(currentLeague.personsCount);
        AssignRectTransform(currentLeague.personsCount, true);
        AssignPersonsInfo(currentLeague);

        _spawnTransformPlayers.position = Vector2.up * _spawnTransformPlayers.GetComponent<RectTransform>().sizeDelta / 2;

        OnChangeLeague.Invoke(currentLeague);
    }
}
