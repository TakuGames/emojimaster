﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


public class LeagueEvent : UnityEvent<LeagueSO> { }
public class PersonEvent : UnityEvent<PersonSO, LeagueSO> { }
public class ScalerCenter : MonoBehaviour
{
    [Serializable]
    public struct ColorSettings
    {
        public Color selectedColor;
        public Color unselectedColor;
        public Color noneAvailableColor;
    }

    public LeagueEvent OnLeagueEvent = new LeagueEvent();
    public PersonEvent OnPersonEvent = new PersonEvent();

    public static PersonSO currentPerson => _currentPerson;

    [SerializeField] private ColorSettings _colorSettings;
    [SerializeField] private Transform _content;
    [SerializeField] private float _scaleFactor;

    private Button[] _items;

    private int _leagueID;
    private int _playerID;

    private float _snappingDistance;
    private float _distance;

    private bool _isDistance;

    private static LeagueSO _currentLeague;
    private static PersonSO _currentPerson;

    private void Start()
    {
        _content.GetComponent<HorizontalLayoutGroup>().enabled = false;
        _items = _content.GetComponentsInChildren<Button>();
        _snappingDistance = GetStep() * 0.5f;
    }
    private void Update()
    {
        for (int i = 0; i < _items.Length; i++)
        {
            CheckingDistance(i);
            ScalingItems(i);
            SelectLeague(i);
            SelectPlayer(i);
        }
    }

    public void Initialization()
    {
        _content.GetComponent<HorizontalLayoutGroup>().enabled = false;
        _items = _content.GetComponentsInChildren<Button>();
        _snappingDistance = GetStep() * 0.5f;
    }

    private void CheckingDistance(int index)
    {
        _distance = Mathf.Abs(transform.position.x - _items[index].transform.position.x);
        _isDistance = (_distance <= _snappingDistance);
    }
    private void ScalingItems(int index)
    {
        var childTransform = _items[index].transform.GetChild(0);
        var scaleFactor = (1 / Mathf.Sqrt(Mathf.Clamp(_distance, 1 - _scaleFactor, 1)));

        childTransform.transform.localScale = (_distance <= _snappingDistance) ? Vector2.one * scaleFactor : Vector2.one;

        if (_isDistance)
            _items[index].GetComponent<RectTransform>().SetAsLastSibling();
    }
    private void SelectLeague(int index)
    {
        var leagueItem = _items[index].GetComponent<LeagueItem>();

        if (_isDistance)
        {
            if (leagueItem)
            {
                if (_leagueID != leagueItem.league.GetInstanceID())
                {
                    OnLeagueEvent.Invoke(leagueItem.league);
                    _leagueID = leagueItem.league.GetInstanceID();
                }

                leagueItem.SelectLeague(_colorSettings.selectedColor);
                _currentLeague = leagueItem.league;
            }
        }
        else
        {
            if (leagueItem)
            {
                leagueItem.SelectLeague(_colorSettings.unselectedColor);
            }
        }
    }
    private void SelectPlayer(int index)
    {
        var playerItem = _items[index].GetComponent<PersonUI>();

        if (_isDistance)
        {
            if (playerItem)
            {
                if (_playerID != playerItem.person.GetInstanceID())
                {
                    OnPersonEvent.Invoke(playerItem.person, _currentLeague);
                    _playerID = playerItem.person.GetInstanceID();

                    PersonUIScroller personUI = (PersonUIScroller)playerItem;
                    ChangeColorPlayer(personUI, _colorSettings.selectedColor);
                    _currentPerson = personUI.person;
                }
            }
        }
        else
        {
            if (playerItem)
            {
                ChangeColorPlayer((PersonUIScroller)playerItem, _colorSettings.unselectedColor);
            }
        }
    }

    private void ChangeColorPlayer(PersonUIScroller personUI, Color selectedColor)
    {
        if (personUI)
        {
            if (_currentLeague.completed)
            {
                personUI.ColorazePerson(selectedColor);
            }
            else
            {
                personUI.ColorazePerson(_currentLeague);
                personUI.ColorazePerson(_colorSettings.noneAvailableColor, 1);
            }
        }
    }

    private float GetStep()
    {
        Vector3[] vec = new Vector3[4];
        RectTransform rect = (RectTransform)transform;
        rect.GetWorldCorners(vec);

        float step = Mathf.Abs(vec[1].y - vec[0].y);
        return step;
    }
}
