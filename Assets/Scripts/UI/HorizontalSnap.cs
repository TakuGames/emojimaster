﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HorizontalSnap : MonoBehaviour
{
    public int direction => _direction;

    [SerializeField] private RectTransform _scrollPanel;
    [SerializeField] private ScrollRect _rectPanel;

    [SerializeField] private float _sensitivity;
    [SerializeField] private float _snapSpeed;

    private bool _draggin;

    private int _direction;

    private float _step;
    private float _prevPosX;
    private float _destinationX;
    private float _snappingDistance;

    private void Start()
    {
        _step = GetStep();
        _snappingDistance = _step * 0.5f;
    }
    private void LateUpdate()
    {
        Check();
        Rolling();
    }

    private void Check()
    {
        if (Math.Abs(_scrollPanel.transform.position.x - _prevPosX) >= _step - _snappingDistance)
        {
            if (direction == 0)
                return;

            _prevPosX += direction * _step;
            _destinationX = _prevPosX;
        }
    }
    private void Rolling()
    {
        if (_draggin)
        {
            _direction = (_rectPanel.velocity.x > 0) ? 1 : -1;
            return;
        }

        if (Mathf.Abs(_rectPanel.velocity.x) > _sensitivity)
            return;

        var currentScrollPos = _scrollPanel.position;
        currentScrollPos.x = Mathf.MoveTowards(_scrollPanel.position.x, _destinationX, Time.deltaTime * _snapSpeed);
        _scrollPanel.position = currentScrollPos;
    }
    private float GetStep()
    {
        Vector3[] vec = new Vector3[4];
        _scrollPanel.GetWorldCorners(vec);

        float step = Mathf.Abs(vec[1].y - vec[0].y);
        return step;
    }


    public void Initialization()
    {
        _step = GetStep();
        _snappingDistance = _step * 0.5f;
    }
    public void ChangeScrollPosX(float posX)
    {
        float finalPos = 0;

        finalPos = -posX;
        var pos = _scrollPanel.position;
        pos.x = finalPos;
        //
        _prevPosX = finalPos;
        _destinationX = finalPos;
        _scrollPanel.position = pos;
    }
    public void ChangeScrollPosX(float posX, bool even)
    {
        float finalPos = 0;
        //float finalPos = -posX;

        if (Math.Abs(posX) > 0)
            _direction = -(int)(posX / Math.Abs(posX));

        //Debug.Log($"_direction: {_direction}");

        finalPos = (even) ? -posX : -(posX - (direction * _snappingDistance));

        //Debug.Log($"test: {finalPos}");

        var pos = _scrollPanel.position;
        pos.x = finalPos;

        _prevPosX = finalPos;
        _destinationX = finalPos;
        _scrollPanel.position = pos;
    }
   
    public void Draggin(bool value)
    {
        _draggin = value;
    }
}
