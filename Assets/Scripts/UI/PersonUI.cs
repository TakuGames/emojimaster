﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PersonUI : MonoBehaviour
{
    [Serializable]
    public struct PersonParts
    {
        public Image circle;
        public Image torso;

        public RectTransform head;
        public Image idle;
        public Image joy;
        public Image anger;

        private Image[] _parts;

        public Image[] parts
        {
            get
            {
                if (_parts == null)
                {
                    _parts = new Image[] {circle, torso, idle, joy, anger};
                }

                return _parts;
            }
        }
    }

    [SerializeField] protected PersonParts _personsParts;
    protected PersonSO _person;
    protected Animator _animator;

    public PersonSO person
    {
        get => _person;
        set
        {
            _person = value;
            AssignAvatar();
        }
    }

    public Animator animator
    {
        get
        {
            if (_animator == null)
                _animator = GetComponent<Animator>();

            return _animator;
        }
    }

    //Add check null
    protected virtual void AssignAvatar()
    {
        if (_personsParts.circle != null)
            _personsParts.circle.color = _person.avatar.color;

        if (_personsParts.torso != null)
        {
            _personsParts.torso.sprite = _person.avatar.torso;
            _personsParts.torso.GetComponent<RectTransform>().sizeDelta = _personsParts.torso.sprite.rect.size;
        }

        if (_personsParts.head != null)
            _personsParts.head.sizeDelta = _person.avatar.headIdle.rect.size;
        
        if (_personsParts.idle != null)
            _personsParts.idle.sprite = _person.avatar.headIdle;

        if (_personsParts.joy != null)
        {
            _personsParts.joy.sprite = _person.avatar.headJoy;
            _personsParts.joy.gameObject.SetActive(false);
        }

        if (_personsParts.anger != null)
        {
            _personsParts.anger.gameObject.SetActive(false);
            _personsParts.anger.sprite = _person.avatar.headAnger;
        }
    }
}