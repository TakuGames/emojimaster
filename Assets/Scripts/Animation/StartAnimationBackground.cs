﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartAnimationBackground : AAnimationBackground<StartAnimationBackgroundElement>
{
    [SerializeField] private float _startDelay = 5f;
    [SerializeField] private float _addDelay = 0.5f;
    private EDirection _curDirection;
    private float _curDuration;

    private protected override void Start()
    {
        _curDirection = EDirection.Left;
        _curDuration = _startDelay;
        base.Start();
    }

    protected override StartPlay SetupElement(StartAnimationBackgroundElement element)
    {
        return () =>
        {
            element.direction = GetDirection();
            element.MoveAnimate(_curDuration);
        };
    }

    protected override WaitForSeconds Delay()
    {
        _curDuration = Mathf.Clamp(_curDuration + _addDelay, _startDelay, _delay);
        return new WaitForSeconds(Random.Range(_curDuration * 0.03f, _curDuration * 0.2f));
    }

    private EDirection GetDirection()
    {
        _curDirection = _curDirection == EDirection.Left ? EDirection.Right : EDirection.Left;
        return _curDirection;
    }
}
