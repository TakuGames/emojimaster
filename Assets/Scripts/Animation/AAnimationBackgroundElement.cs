﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public abstract class AAnimationBackgroundElement : MonoBehaviour
{
    protected RectTransform _rt;
    protected bool _isPlay;
    protected Vector2 _sizeObject;
    protected Vector2 _sizeBackground;

    public bool isPlay => _isPlay;

    protected virtual void Awake()
    {
        _rt = GetComponent<RectTransform>();
        _sizeObject = _rt.sizeDelta;

        var rt = (RectTransform) GetComponentInParent<Canvas>().transform;
        _sizeBackground = rt.sizeDelta;
    }

    public virtual void MoveAnimate(float duration)
    {
        _isPlay = true;
        var startPosition = StartPosition();
        var endPosition = EndPosition();

        _rt.localPosition = startPosition;

        var seq = DOTween.Sequence();
        seq.Append(_rt.DOLocalMove(endPosition, duration, true).SetEase(Ease.Linear));
        seq.AppendCallback(() => _isPlay = false);
        seq.Play();
    }

    protected abstract Vector2 StartPosition();
    protected abstract Vector2 EndPosition();
}
