﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishAnimationBackgroundElement : AAnimationBackgroundElement
{
    private Vector3 _startScale;
    private float _x;
    private float _y;
    
    public bool upwards { get; set; }

    protected override void Awake()
    {
        base.Awake();
        _startScale = _rt.localScale;
    }

    protected override Vector2 StartPosition()
    {
        _x = _sizeBackground.x * 0.6f;
        _y = (_sizeBackground.y * -0.5f) - _sizeObject.y;
        _x = Random.Range(-_x, _x);

        _y = upwards ? _y : -_y;
        
        return new Vector2(_x, _y);
    }

    protected override Vector2 EndPosition()
    {
        return new Vector2(_x,-_y);
    }

    public void RandomScale(float changeScale)
    {
        var newScale = _startScale;
        float addValue = Random.Range(-changeScale, changeScale);
        newScale.x += addValue;
        newScale.y += addValue;
        _rt.localScale = newScale;
    }
}
