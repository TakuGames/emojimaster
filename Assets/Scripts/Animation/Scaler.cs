﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Scaler : MonoBehaviour
{
    [SerializeField] private float _startScale = 0.2f;
    [SerializeField] private float _duration = 2f;

    private void Start()
    {
        var oldScale = transform.localScale;
        var startScale = new Vector2(_startScale, _startScale);
        transform.localScale = startScale;
        transform.DOScale(oldScale, _duration);
    }
}
