﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartAnimationBackgroundElement : AAnimationBackgroundElement
{
    public EDirection direction { get; set; }

    private float _x;
    private float _y;

    protected override Vector2 StartPosition()
    {
        _x = (_sizeBackground.x * 0.5f) - (_sizeObject.x * 0.5f) - 20f;
        _y = (_sizeBackground.y * -0.5f) - _sizeObject.y;
        
        Vector3 rotation = Vector3.zero;

        if (direction == EDirection.Left)
        {
            rotation = new Vector3(0f, 180f, 0f);
            _x = -_x;
        }
        
        _rt.rotation = Quaternion.Euler(rotation);
        
        return new Vector2(_x, _y);
    }

    protected override Vector2 EndPosition()
    {
        return new Vector2(_x,-_y);
    }
}
