﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sprites : MonoBehaviour
{
    [SerializeField] private Sprite[] _sprites;
    [SerializeField] private float _delay = 0.2f;
    [SerializeField] private bool _random = false;

    private Image _image;
    private float _curDelay = 0;
    private int _index = 0;
    private float _counter = 0;

    private void Awake()
    {
        _image = GetComponent<Image>();
    }

    private void Update()
    {
        if (_curDelay <= 0)
        {
            if (!_random)
            {
                ChangeSprite();
            }
            else
            {
                ChangeRandomSprite();
            }

            _curDelay = _delay;
        }

        _curDelay -= Time.deltaTime;
    }

    private void ChangeSprite()
    {
        _image.sprite = _sprites[_index];
        _counter++;
        _index = (int) Mathf.Repeat(_counter, _sprites.Length);
    }

    private void ChangeRandomSprite()
    {
        _image.sprite = _sprites[Random.Range(0, _sprites.Length)];
    }
}