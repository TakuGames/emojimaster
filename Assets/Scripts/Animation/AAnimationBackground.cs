﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public abstract class AAnimationBackground<T> : MonoBehaviour
{
    protected delegate void StartPlay();
    
    [SerializeField] private GameObject _prefabElement;
    [SerializeField] protected float _delay = 20f;

    protected Vector2 _sizeDelta;
    protected List<T> _elements;

    private T _getElement
    {
        get
        {
            T selected = default(T);

            foreach (var element in _elements)
            {
                var property = element.GetType().GetProperty("isPlay");
                if (property == null)
                    break;
                
                var value = (bool)property.GetValue(element);
                if (value != false) continue;
                selected = element;
                break;
            }

            if (selected == null)
            {
                selected = Instantiate(_prefabElement, gameObject.transform).GetComponent<T>();
                _elements.Add(selected);
            }

            return selected;
        }
    }

    private protected virtual void Start()
    {
        var rt = (RectTransform) GetComponentInParent<Canvas>().transform;
        _sizeDelta = rt.sizeDelta;
        _elements = new List<T>();
        StartCoroutine(Play());
    }

    private IEnumerator Play()
    {
        while (true)
        {
            StartPlay play = SetupElement(_getElement);
            play();
            yield return Delay();
        }
    }

    protected abstract StartPlay SetupElement(T element);
    protected abstract WaitForSeconds Delay();
}