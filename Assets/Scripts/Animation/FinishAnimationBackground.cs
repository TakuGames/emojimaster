﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishAnimationBackground : AAnimationBackground<FinishAnimationBackgroundElement>
{
    [SerializeField] private float _changeScale = 0.5f;
    [SerializeField] private float _baseDurationMoveElement = 20f;
    [SerializeField] private float _changeDurationMoveElement = 5f;
    [SerializeField] private bool _upwards = true;
    private WaitForSeconds _wait;

    private protected override void Start()
    {
        _wait = new WaitForSeconds(_delay);
        base.Start();
    }

    protected override StartPlay SetupElement(FinishAnimationBackgroundElement element)
    {
        return () =>
        {
            element.upwards = _upwards;
            element.RandomScale(_changeScale);
            element.MoveAnimate(RandomDurationMoveElement());
        };
    }

    protected override WaitForSeconds Delay()
    {
        return _wait;
    }

    private float RandomDurationMoveElement()
    {
        var duration = _baseDurationMoveElement + Random.Range(-_changeDurationMoveElement, _changeDurationMoveElement);
        return duration;
    }
}
