﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/Combo", fileName = "NewRulesCombo")]
public class ComboRuleSO : AComboSO
{
    public int countTouch => _countTouch;
    public int multiply => _plusCount;

    [SerializeField] private string _ruleName;
    [SerializeField] private int _plusCount;
    [SerializeField] private int _countTouch;

    protected override bool RulesCombo(int count)
    {
        if (_countTouch <= count)
        {
            Debug.Log($"{_ruleName} add to score +{_plusCount}");
        }
        bool result = (_countTouch > count);
        return result;       
    }
}
