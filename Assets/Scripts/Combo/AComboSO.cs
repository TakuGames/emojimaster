﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AComboSO : ScriptableObject
{
    public bool CheckCombo(int jumpCount)
    {
        return RulesCombo(jumpCount);
    }
    protected abstract bool RulesCombo(int count);
}
