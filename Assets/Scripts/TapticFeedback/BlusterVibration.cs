﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public enum EVibrationType
{
    Warning, Failure, Success, Light, Medium, Heavy, Default, Vibrate, Selection
}

public class BlusterVibration : MonoBehaviour
{
    private void Update()
    {
        SetVibration(5); // Heavy
    }

    public void SetVibration(int type)
    {
        var currentType = (EVibrationType)type;
        Taptic.SetVibration(currentType);
    }
}
