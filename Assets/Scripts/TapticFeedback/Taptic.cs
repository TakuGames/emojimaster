﻿using UnityEngine;
using UnityEngine.UI;
using System.Reflection;
using System.Runtime.InteropServices;

#if UNITY_EDITOR
using UnityEditor;
#endif

public static class Taptic
{

#if UNITY_IOS
        [DllImport("__Internal")]
        private static extern void _PlayTaptic(string type);
        [DllImport("__Internal")]
        private static extern void _PlayTaptic6s(string type);
#endif

    private static bool _tapticOn = true;

    public static bool IsTaptic
    {
        set
        {
            _tapticOn = value;
            PlayerPrefs.SetString("Vibrate", _tapticOn ? "true" : "false");
        }
        get
        {
            return _tapticOn;
        }
    }

    public static void Warning()
    {
        if (!_tapticOn || Application.isEditor)
            return;

#if UNITY_IOS
        if (iPhone6s())
        {
            _PlayTaptic6s("warning");
        }
        else
        {
            _PlayTaptic("warning");
        }

#elif UNITY_ANDROID
        AndroidTaptic.Haptic(HapticTypes.Warning);
#endif
    }

    public static void Failure()
    {
        if (!_tapticOn || Application.isEditor)
        {
            return;
        }
#if UNITY_IOS
                if (iPhone6s()) {
                        _PlayTaptic6s("failure");
                } else {
                        _PlayTaptic("failure");
                }
#elif UNITY_ANDROID
        AndroidTaptic.Haptic(HapticTypes.Failure);
#endif
    }

    public static void Success()
    {
        if (!_tapticOn || Application.isEditor)
        {
            return;
        }
#if UNITY_IOS
                if (iPhone6s()) {
                        _PlayTaptic6s("success");
                } else {
                        _PlayTaptic("success");
                }
#elif UNITY_ANDROID
        AndroidTaptic.Haptic(HapticTypes.Success);
#endif
    }

    public static void Light()
    {
        //Debug.Log("Taptic Light");
        
        if (!_tapticOn || Application.isEditor)
        {
            return;
        }

#if UNITY_IOS
                if (iPhone6s()) {
                        _PlayTaptic6s("light");
                } else {
                        _PlayTaptic("light");
                }
#elif UNITY_ANDROID
        AndroidTaptic.Haptic(HapticTypes.LightImpact);
#endif
    }
    public static void Medium()
    {
        //Debug.Log("Taptic Medium");
        
        if (!_tapticOn || Application.isEditor)
        {
            return;
        }
#if UNITY_IOS
                if (iPhone6s()) {
                        _PlayTaptic6s("medium");
                } else {
                        _PlayTaptic("medium");
                }
#elif UNITY_ANDROID
        AndroidTaptic.Haptic(HapticTypes.MediumImpact);
#endif
    }
    public static void Heavy()
    {
        //Debug.Log("Taptic Heavy");
        
        if (!_tapticOn || Application.isEditor)
        {
            return;
        }
#if UNITY_IOS
                if (iPhone6s()) {
                        _PlayTaptic6s("heavy");
                } else {
                        _PlayTaptic("heavy");
                }
#elif UNITY_ANDROID
        AndroidTaptic.Haptic(HapticTypes.HeavyImpact);
#endif
    }

    public static void Default()
    {
        if (!_tapticOn || Application.isEditor)
        {
            return;
        }
#if UNITY_IOS || UNITY_ANDROID
        Handheld.Vibrate();
#endif
    }

    public static void Vibrate()
    {
        if (!_tapticOn || Application.isEditor)
        {
            return;
        }
#if UNITY_IOS
                if (iPhone6s()) {
                        _PlayTaptic6s("medium");
                } else {
                        _PlayTaptic("medium");
                }
#elif UNITY_ANDROID
        AndroidTaptic.Vibrate();
#endif
    }

    public static void Selection()
    {
        if (!_tapticOn || Application.isEditor)
        {
            return;
        }
#if UNITY_IOS
                if (iPhone6s()) {
                        _PlayTaptic6s("selection");
                } else {
                        _PlayTaptic("selection");
                }
#elif UNITY_ANDROID
        AndroidTaptic.Haptic(HapticTypes.Selection);
#endif
    }

    public static void SetVibration(EVibrationType type)
    {
        MethodInfo method = typeof(Taptic).GetMethod(type.ToString());

        if (method != null)
        {
            method.Invoke(new object(), null);
            Debug.Log($"{method.GetHashCode()} {method.Name}");
            return;
        }
    }

    static bool iPhone6s()
    {
        return SystemInfo.deviceModel == "iPhone8,1" || SystemInfo.deviceModel == "iPhone8,2";
    }

}