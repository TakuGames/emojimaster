﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathBottomController : MonoBehaviour
{
    private void Start()
    {
        Transform[] allChilds = new Transform[transform.childCount];

        for (int i = 0; i < allChilds.Length; i++)
        {
            allChilds[i] = transform.GetChild(i);
        }

        foreach (var child in allChilds)
        {
            DestroyImmediate(child.gameObject);
        }

        Instantiate(StateGame.opponent.league.deathBottomPrefab, transform);
    }
}
