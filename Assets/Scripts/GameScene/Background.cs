﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    private void Start()
    {
        var newSprite = StateGame.opponent.league.background;
        if (newSprite == null)
            return;

        GetComponent<SpriteRenderer>().sprite = newSprite;
    }
}
