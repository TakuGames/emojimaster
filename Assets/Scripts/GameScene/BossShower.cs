﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossShower : MonoBehaviour
{
    [SerializeField] private float _minDelaySecond = 5f;
    [SerializeField] private float _delaySekondAddLike = 0.5f;
    [SerializeField] private float _delaySekondFirstLike = 2;
    private PersonUI _bossUI;
    private PersonSO _opponentData;

    private WaitForSeconds _delay;
    private WaitForSeconds _delayAddLike;
    private WaitForSeconds _delayFirstLike;
    private int _likeCount = 1; 

    private void Awake()
    {
        _bossUI = GetComponentInChildren<PersonUI>();
    }

    private void Start()
    {
        _opponentData = StateGame.opponent.personData;
        if (_opponentData.power.additionsLikesInMinute <= 0)
        {
            Destroy(gameObject);
            return;
        }

        var bossData = StateGame.opponent.league.GetBoss();
        Debug.Log(bossData.name);
        _bossUI.person = bossData;
        _delayAddLike = new WaitForSeconds(_delaySekondAddLike);
        _delayFirstLike = new WaitForSeconds(_delaySekondFirstLike);
        
        AssignLikesForTime();
        StartCoroutine(PeriodicAppearanceBoss());
    }

    private IEnumerator PeriodicAppearanceBoss()
    {
        while (true)
        {
            yield return _delay;
            _bossUI.animator.SetBool("Show", true);
            StartCoroutine(PeriodicAppearanceLikes());
        }
    }

    private IEnumerator PeriodicAppearanceLikes()
    {
        yield return _delayFirstLike;
        
        for (int i = 0; i < _likeCount; i++)
        {
            _bossUI.animator.SetTrigger("Like");
            _opponentData.score++;
            CoreSystem.vfXController.PlayEffectOnes("Like", _opponentData.name, gameObject.transform.position);
            yield return _delayAddLike;
        }
        
        _bossUI.animator.SetBool("Show", false);
    }

    private void AssignLikesForTime()
    {
        var power = _opponentData.power;
        float delaySecond = 60f / (float)power.additionsLikesInMinute;

        while (delaySecond <= _minDelaySecond)
        {
            delaySecond *= 2;
            _likeCount *= 2;
        }
        
        _delay = new WaitForSeconds(delaySecond + _delaySekondAddLike * _likeCount);
        
        Debug.Log($"delay: {delaySecond + _delaySekondFirstLike + _delaySekondAddLike * (_likeCount - 1)}   likes: {_likeCount}");
    }
}
