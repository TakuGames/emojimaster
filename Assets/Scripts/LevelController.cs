﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DG.Tweening;

public class LevelController : MonoBehaviour
{
    public EDirection lastDirection
    {
        get => _lastDirection;
        set => _lastDirection = value;
    }
    public Vector2 firstMesagePos => _firstMesagePos;
    public int contentCount => _contentQueue.Count;

    private Queue<AContent> _contentQueue = new Queue<AContent>();

    private ContentPull _messagesPull;
    private ContentPull _stickersPull;

    private EDirection _lastDirection = EDirection.Left;
    private AContent _currentContent;

    private Vector2 _firstMesagePos;
    private Ease _levelEase;

    private int _countUniqueMessage;

    private float _indentX;
    private float _indentY;
    private float _duration;
    private float _halfWidthLevel;
    private float _halfHeightLevel;
    private float _contentsDistance;
    private float _deactivationDistance;

    private void Start()
    {
        var camera = Camera.main;

        _messagesPull = new ContentPull("Message");
        _stickersPull = new ContentPull("Sticker");

        var orthographicSize = camera.orthographicSize;

        _halfWidthLevel = orthographicSize * camera.aspect;
        _halfHeightLevel = orthographicSize - camera.transform.position.y;

        _deactivationDistance = orthographicSize * 2f;

        _countUniqueMessage = CoreSystem.gameData.contentSetup.startCountUniqueMessage;
        _levelEase = CoreSystem.gameData.gameSetup.levelEase;
        _duration = CoreSystem.gameData.gameSetup.duration;
        _indentX = CoreSystem.gameData.gameSetup.indentX;
        _indentY = CoreSystem.gameData.gameSetup.indentY;

        SpawnMessages();
    }
    private void SpawnMessages()
    {
        _currentContent = (_countUniqueMessage > 0) ? AddContent(true) : AddContent();

        Vector2 startPos = transform.position;
        Vector2 endPos = startPos + Vector2.up * (_currentContent.size.y + _indentY);

        transform.DOMove(endPos, _duration).SetEase(_levelEase).OnComplete(MoveCompleted);
    }
    private void AssignPosition(AContent content)
    {
        content.transform.parent = transform;

        var offset = _halfWidthLevel - (content.size.x * 0.5f) - _indentX;
        var newLocalPosition = Vector3.zero;

        newLocalPosition.x = content.direction == EDirection.Right ? offset : -offset;
        newLocalPosition.y = -(_halfHeightLevel + content.size.y * 0.5f);

        content.transform.localPosition = newLocalPosition - transform.localPosition;

        _contentsDistance += (content.size.y + _indentY);

        if (_firstMesagePos.sqrMagnitude <= 0)
            _firstMesagePos = content.transform.position;
    }
    private void CheckDeactivating(AContent content)
    {
        _contentQueue.Enqueue(content);

        while (_contentsDistance > _deactivationDistance)
        {
            var currentContent = _contentQueue.Dequeue();
            _contentsDistance -= (currentContent.size.y + _indentY);
            currentContent.Deactivate();
        }
    }
    private void MoveCompleted()
    {
        _countUniqueMessage--;

        CheckDeactivating(_currentContent);
        SpawnMessages();
    }

    private AContent AddContent(bool startContent = false)
    {
        AContent content;

        _duration = (startContent) ?
            CoreSystem.gameData.gameSetup.startSpeed :
            CoreSystem.gameData.gameSetup.duration;

        if (startContent)
        {
            content = _messagesPull.GetContent();
            content.Generate(EStyleContent.Start);
        }
        else
        {
            content = Random.Range(0, 100) <= CoreSystem.gameData.contentSetup.chanceMessage
                ? _messagesPull.GetContent()
                : _stickersPull.GetContent();
            content.Generate();
        }

        AssignPosition(content);
        return content;
    }
}