﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdderLikes : MonoBehaviour
{
    [SerializeField] private List<ComboRuleSO> _rules = new List<ComboRuleSO>();

    private bool _isAddLikes = true;

    private int _ruleID;
    private int _contentId;
    private int _touchCount;

    private Coroutine _delayCoroutine;

    IEnumerator DelayingAdded(AContent content, string effectName)
    {
        var finalCount = _rules[_ruleID].multiply + 1;
        var waiter = new WaitForSeconds(0.08f);

        for (int i = 0; i < finalCount; i++)
        {
            yield return waiter;
            content.person.AddScore(content.sent);
            CoreSystem.vfXController.PlayEffectOnes(effectName, content.person.name, transform.position);
        }

        _touchCount = 0;
        _delayCoroutine = null;
    }

    public void AddLike(Collider2D collider, Vector2 origin, string like, string dislike)
    {
        if (!_isAddLikes)
            return;

        var content = collider.GetComponent<AContent>();

        if (content == null)
            return;

        _touchCount = _contentId.Equals(content.GetInstanceID()) ? ++_touchCount : 1;
        _contentId = content.GetInstanceID();

        // допилить красиво 
        for (int i = 0; i < _rules.Count; i++)
        {
            if (_rules[i].countTouch == _touchCount)
                _ruleID = i;
        }

        if (_rules[_ruleID].CheckCombo(_touchCount))
        {
            content.person.AddScore(content.sent);
            CoreSystem.vfXController.PlayEffectOnes(content.sent == ESent.Good ? like : dislike, content.person.name, origin);
        }
        else
        {
            string effectName = content.sent == ESent.Good ? like : dislike;

            if (_delayCoroutine == null)
                _delayCoroutine = StartCoroutine(DelayingAdded(content, effectName)); 
        }
    }

    public void SetAddingLikes(bool value)
    {
        _isAddLikes = value;
    }
}
