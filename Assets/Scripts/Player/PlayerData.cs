﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public struct PlayerMove
{
    public float speed;
    [Range(1, 3)] public float sensitivityX;
}

[Serializable]
public struct PlayerJump
{
    public ForceMode2D mode;
    public string nameToLayer;
    public float jumpForce;
}

[CreateAssetMenu(menuName = "Data/PlayerData", fileName = "NewPlayerData")]
public class PlayerData : ScriptableObject
{
    public PlayerMove playerMove;
    public PlayerJump playerJump;
}
