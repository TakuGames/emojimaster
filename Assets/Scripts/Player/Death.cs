﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public interface IDie
{
    void Die(bool particle);
}

public class Death : MonoBehaviour, IDie
{
    [SerializeField] private ParticleSystem _fxDeath;
    public UnityEvent dieEvent = new UnityEvent(); 

    public void Die(bool particle = false)
    {
        if (particle && _fxDeath)
            Instantiate(_fxDeath, gameObject.transform.position, Quaternion.identity);

        dieEvent.Invoke();
        StartCoroutine(Restart(_fxDeath.main.duration));
        Destroy(GetComponent<SpriteRenderer>());
        Destroy(GetComponent<Collider2D>());
        Destroy(GetComponent<PlayerController>());
    }

    private IEnumerator Restart(float delay)
    {
        var pause = FindObjectOfType<PauseController>();

        if (pause) Destroy(pause);

        var delaySec = new WaitForSeconds(delay);
        yield return delaySec;
        
        StateGame.Lose();
        LoadScene.LoadSceneByIndex(2);
    }
}
