﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private PlayerData _playerData;

    private int _mask;
    private float _sensitivityX;
    private float _fingerPosX;
    private float _playerX;
    private float _clampX;
    private Vector2 _origin;

    private Camera _mainCamera;
    private Rigidbody2D _rb;
    private Collider2D _collider;

    private AdderLikes _adder;

    private const string LIKE = "Like";
    private const string DIZLIKE = "Dislike";
    private float _contactDistance;

    //private bool _isAddLikes = true;

    private void Awake()
    {
        _mainCamera = Camera.main;
        _rb = GetComponent<Rigidbody2D>();
        _collider = GetComponent<Collider2D>();
        _adder = GetComponent<AdderLikes>();
        _sensitivityX = _playerData.playerMove.sensitivityX;
    }
    private void Start()
    {
        _mask = 1 << LayerMask.NameToLayer(_playerData.playerJump.nameToLayer);
        _contactDistance = transform.localScale.y * 0.5f + 0.01f;

        var levelController = FindObjectOfType<LevelController>();
        transform.parent = levelController.transform;

        var startPos = transform.position;
        startPos.x = levelController.firstMesagePos.x;
        transform.position = startPos;

        var collider = GetComponent<Collider2D>();
        _clampX = Camera.main.orthographicSize * Camera.main.aspect - collider.bounds.size.x * 0.5f;
    }
    private void LateUpdate()
    {
        Move();
        Jump();
    }

    private void Move()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _fingerPosX = _mainCamera.ScreenToWorldPoint(Input.mousePosition).x;
            _playerX = transform.position.x;
        }

        if (Input.GetMouseButton(0))
        {
            var currentFingerX = _mainCamera.ScreenToWorldPoint(Input.mousePosition).x;
            var pos = transform.position;
            pos.x = _playerX + (currentFingerX - _fingerPosX) * _sensitivityX;
            transform.position = Vector2.Lerp(transform.position, pos, _playerData.playerMove.speed);

            var currentPos = transform.position;
            currentPos.x = Mathf.Clamp(transform.position.x, -_clampX, _clampX);
            transform.position = currentPos;
        }
    }
    private void Jump()
    {
        if (_rb.velocity.y > 0)
            return;

        float[] pos = new float[] { _collider.bounds.center.x,
            _collider.bounds.min.x,
            _collider.bounds.max.x};

        for (int i = 0; i < pos.Length; i++)
        {
            _origin = new Vector2(pos[i], _collider.bounds.center.y);

            var hit = Physics2D.Raycast(_origin, Vector2.down, Mathf.Infinity, _mask);

            if (!hit.collider)
                return;

            if (hit.distance <= _contactDistance)
            {
                _rb.velocity = Vector2.zero;
                _rb.AddForce(Vector2.up * _playerData.playerJump.jumpForce, _playerData.playerJump.mode);

                _adder.AddLike(hit.collider, _origin, LIKE, DIZLIKE);

                //AddLike(hit.collider, _origin);

                break;
            }
        }
    }

    private void AddLike(Collider2D collider, Vector2 origin)
    {
        //if (!_isAddLikes) return;

        var content = collider.GetComponent<AContent>();
        if (content == null)
            return;

        content.person.AddScore(content.sent);

        CoreSystem.vfXController.PlayEffectOnes(content.sent == ESent.Good ? LIKE : DIZLIKE, content.person.name, origin);

        //        Debug.Log($"Left - like: {CoreSystem.opponentController.GetPerson(EDirection.Left).score.like}, dizlike: {CoreSystem.opponentController.GetPerson(EDirection.Left).score.dislike}   |   " +
        //             $"Right - like: {CoreSystem.opponentController.GetPerson(EDirection.Right).score.like}, dizlike: {CoreSystem.opponentController.GetPerson(EDirection.Right).score.dislike}");
    }

    public void SetAddingLikes(bool value)
    {
        //_isAddLikes = value;
    }
}