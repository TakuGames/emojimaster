﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationDeathBottom : MonoBehaviour
{
    [SerializeField] private float _delayParticleSecond = 0.5f;
    [SerializeField] private BoxCollider2D _border;
    private WaitForSeconds _delay;
    private Animator[] _particles;

    private float _leftBorder;
    private float _rightBorder;

    private void Start()
    {
        _particles = GetComponentsInChildren<Animator>(true);
        foreach (var particle in _particles)
        {
            particle.gameObject.SetActive(false);
        }
        
        _delay = new WaitForSeconds(_delayParticleSecond);

        _leftBorder = _border.bounds.min.x;
        _rightBorder = _border.bounds.max.x;
        
        StartCoroutine(Play());
    }

    private IEnumerator Play()
    {
        while (true)
        {
            foreach (var particle in _particles)
            {
                if (!particle.gameObject.activeSelf)
                {
                    particle.gameObject.SetActive(true);
                    var newLocalPos = particle.transform.localPosition;
                    newLocalPos.x = Random.Range(_leftBorder, _rightBorder);
                    particle.transform.localPosition = newLocalPos;
                    break;
                }
            }

            yield return _delay;
        }
    }
}
