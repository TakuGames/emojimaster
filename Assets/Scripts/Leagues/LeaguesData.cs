﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;



[CreateAssetMenu(fileName = "LeaguesData", menuName = "Dataset/LeaguesData")]
public class LeaguesData : ScriptableObject
{
    [SerializeField] private LeagueSO _tested;
    [SerializeField] private LeagueSO _single;
    [SerializeField] private LeagueSO[] _leagues;
    
    public LeagueSO tested => _tested;
    public LeagueSO single => _single;
    public LeagueSO[] leagues => _leagues;
    
    public LeagueSO GetLeague(string name)
    {
        if (String.Equals(name, _single.name))
            return _single;
        
        foreach (var league in _leagues)
        {
            if (String.Equals(league.name, name))
                return league;
        }

        return null;
    }

    public LeagueSO GetLeague(int index)
    {
        return _leagues[index];
    }

    public int leaguesCount => _leagues.Length;

    public LeagueSO GetAccessibleLeague()
    {
        foreach (var league in _leagues)
        {
            if (!league.completed)
                return league;
        }

        return null;
    }
}
