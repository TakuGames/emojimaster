﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(fileName = "League", menuName = "Dataset/League")]
public class LeagueSO : ScriptableObject
{
    [SerializeField] private string _name;
    [SerializeField] private Color _color;
    [SerializeField] private GameObject _deathBottomPrefab;
    [SerializeField] private Sprite _background;
    [SerializeField] private Sprite _descriptionBack;
    [SerializeField] private Sprite _logo;
    [SerializeField] private int _reward;
    [SerializeField] protected PersonSO[] _persons;

    [SerializeField] private bool _completed;

    public Color color => _color;
    public GameObject deathBottomPrefab => _deathBottomPrefab;
    public Sprite background => _background;
    public Sprite descriptionBack => _descriptionBack;

    public Sprite logo => _logo;
    public int reward => _reward;
    public string name => _name;

    public bool completed
    {
        get { return _completed; }
        set { _completed = value; }
    }

    public PersonSO[] persons => _persons;
    public int personsCount => _persons.Length;

    public PersonSO GetPerson(int index)
    {
        if (index >= _persons.Length)
            return null;

        return _persons[index];
    }

    public PersonSO GetPerson(string name)
    {
        foreach (var person in _persons)
        {
            if (String.Equals(person.name, name))
                return person;
        }

        return null;
    }

    public PersonSO GetRandomPerson()
    {
        var person = _persons[Random.Range(0, _persons.Length)];
        person.score = new Score(true);
        return person;
    }

    public PersonSO GetOpponent()
    {
        for (int i = 0; i < _persons.Length; i++)
        {
            if (!_persons[i].defeated)
                return _persons[i];
        }

        return null;
    }

    public PersonSO GetBoss()
    {
        return _persons[_persons.Length - 1];
    }

    public int GetNotDefeatedCount()
    {
        int count = personsCount;
        foreach (var person in _persons)
        {
            if (person.defeated)
                count--;
            else
                break;
        }

        return count;
    }
}