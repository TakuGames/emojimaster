﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ColorLeague : MonoBehaviour
{
    private Image _image;
    private SpriteRenderer _sr;

    private void Awake()
    {
        _image = GetComponent<Image>();
        _sr = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        var color = StateGame.opponent.league.color;

        if (_image != null)
            _image.color = color;

        if (_sr != null)
            _sr.color = color;
    }
}
