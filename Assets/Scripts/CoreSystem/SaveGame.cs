﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;
using Object = UnityEngine.Object;


//https://jsoneditoronline.org/

public static class SaveGame
{
    private class SaveGameData
    {
        public League single;
        public League[] leagues;
    }

    private class Person
    {
        public string name;
        public int advertising;
        public bool acquired;
        public bool defeated;
        public int additionsStartLikes;

        public Person(string name, int advertising, bool acquired, bool defeated, int additionsStartLikes)
        {
            this.name = name;
            this.advertising = advertising;
            this.acquired = acquired;
            this.defeated = defeated;
            this.additionsStartLikes = additionsStartLikes;
        }
    }

    private class League
    {
        public string name;
        public bool completed;
        public Person[] persons;

        public int personsCount => persons.Length;

        public League(string name, bool completed, Person[] persons)
        {
            this.name = name;
            this.completed = completed;
            this.persons = persons;
        }
    }

    private static string _path;

    private static string _getPath
    {
        get
        {
            if (String.IsNullOrEmpty(_path))
            {
                _path = GetPath();
            }

            return _path;
        }
    }

    private static string GetPath()
    {
        string path;
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
        path = Path.Combine(Application.persistentDataPath, "SaveData.json");
#else
        path = Path.Combine(Application.dataPath, "SaveData.json");
#endif
        return path;
    }

    public static void SaveData()
    {
        SaveGameData saveGameData = new SaveGameData();

        //save single
        var singleData = StateGame.data.single;
        Person[] singlePersonsSaved = new Person[singleData.personsCount];

        for (int i = 0; i < singlePersonsSaved.Length; i++)
        {
            var personData = singleData.persons[i];
            singlePersonsSaved[i] = new Person(personData.name, 0, personData.price.acquired, false, personData.power.additionsStartLikes);
        }

        saveGameData.single = new League(singleData.name, true, singlePersonsSaved);


        //save leagues
        var leaguesData = StateGame.data.leagues;
        saveGameData.leagues = new League[leaguesData.Length];

        for (int i = 0; i < leaguesData.Length; i++)
        {
            var leagueData = leaguesData[i];
            var personsData = leagueData.persons;
            Person[] personsSaved = new Person[personsData.Length];

            for (int j = 0; j < personsData.Length; j++)
            {
                var personData = personsData[j];
                personsSaved[j] = new Person(personData.name, personData.price.advertising, personData.price.acquired, personData.defeated, personData.power.additionsStartLikes);
            }

            saveGameData.leagues[i] = new League(leagueData.name, leagueData.completed, personsSaved);
        }

        string content = JsonConvert.SerializeObject(saveGameData);
        File.WriteAllText(_getPath, content);

        Debug.Log("Save");
    }

    public static void LoadData()
    {
        if (!File.Exists(_getPath))
        {
            SaveData();
            return;
        }

        SaveGameData saveGameData = JsonConvert.DeserializeObject<SaveGameData>(File.ReadAllText(_getPath));

        //load single
        var singleData = StateGame.data.single;

        int personsCount = saveGameData.single.personsCount < singleData.personsCount
            ? saveGameData.single.personsCount
            : singleData.personsCount;

        for (int i = 0; i < personsCount; i++)
        {
            singleData.persons[i].price.acquired = saveGameData.single.persons[i].acquired;
        }

        //load league
        var leaguesData = StateGame.data.leagues;
        
        var leaguesCount = saveGameData.leagues.Length < leaguesData.Length
            ? saveGameData.leagues.Length
            : leaguesData.Length;

        for (int i = 0; i < leaguesCount; i++)
        {
            leaguesData[i].completed = saveGameData.leagues[i].completed;

            personsCount = saveGameData.leagues[i].personsCount < leaguesData[i].personsCount
                ? saveGameData.leagues[i].personsCount
                : leaguesData[i].personsCount;
            
            for (int j = 0; j < personsCount; j++)
            {
                leaguesData[i].persons[j].price.advertising = saveGameData.leagues[i].persons[j].advertising;
                leaguesData[i].persons[j].price.acquired = saveGameData.leagues[i].persons[j].acquired;
                leaguesData[i].persons[j].defeated = saveGameData.leagues[i].persons[j].defeated;
                leaguesData[i].persons[j].power.additionsStartLikes = saveGameData.leagues[i].persons[j].additionsStartLikes;
            }
        }

        Debug.Log("Load");
    }
}