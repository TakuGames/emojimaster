﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PauseController : MonoBehaviour
{
    private Image _image;
    private TextMeshProUGUI _txt;
    private float _time = 1;

    private void Awake()
    {
        _image = GetComponent<Image>();
        _txt = GetComponentInChildren<TextMeshProUGUI>();
    }

    private void Update()
    {
        _time = (Input.GetMouseButton(0)) ? _time += Time.unscaledDeltaTime * 2 : _time -= Time.unscaledDeltaTime;
        _time = Mathf.Clamp(_time, 0.05f, 1);

        Time.timeScale = _time;
        Time.fixedDeltaTime = Time.deltaTime;

        float alfa = (1 - _time) * 0.5f;

        _image.color = new Color(0, 0, 0, alfa);
        _txt.color = new Color(1, 1, 1, alfa * 2);
    }

    private void OnDestroy()
    {
        Time.timeScale = 1;
    }
}
