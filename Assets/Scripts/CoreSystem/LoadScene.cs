﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class LoadScene
{
    private static int _sceneIndex;
    public static int sceneIndex => _sceneIndex;
    
    public static void LoadSceneByIndex(int sceneIndex, bool withTransition = true)
    {
        _sceneIndex = sceneIndex;
        
        if (!withTransition)
        {
            SceneManager.LoadScene(_sceneIndex);
            return;
        }
        
        LoadSceneWithTransition();
    }

    public static void LoadStartScene(bool withTransition = true)
    {
        _sceneIndex = 0;
        
        if (!withTransition)
        {
            SceneManager.LoadScene(_sceneIndex);
            return;
        }
        
        LoadSceneWithTransition();
    }

    private static void LoadSceneWithTransition()
    {
        var transitionLoadScene = Object.FindObjectOfType<TransitionLoadScene>();
        if (transitionLoadScene == null)
        {
            SceneManager.LoadScene(_sceneIndex);
            return;
        }

        var animator = transitionLoadScene.GetComponent<Animator>();
        if (animator == null)
        {
            SceneManager.LoadScene(_sceneIndex);
            return;
        }
        
        animator.SetTrigger("Play");
    }
}
