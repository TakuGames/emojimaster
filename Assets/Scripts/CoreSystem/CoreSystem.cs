﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoreSystem : Singleton<CoreSystem>, ISerializationCallbackReceiver
{
    [SerializeField] private GameDataSO _gameDataSerialize;
    //[SerializeField] private PersonsDataSO _personsDataSerialize;

    private static GameDataSO _gameData;
    //private static PersonsDataSO _personsData;
    private static LevelController _levelController;
    private static OpponentController _opponentController;
    private static VFXController _vfXController;
    private static GameTimer _timer;


    public static GameDataSO gameData => _gameData;
    //public static PersonsDataSO personsData => _personsData;
    public static LevelController levelController => ValueCheck(ref _levelController);
    public static OpponentController opponentController => ValueCheck(ref _opponentController);
    public static VFXController vfXController => ValueCheck(ref _vfXController);
    public static GameTimer timer => ValueCheck(ref _timer);


    private static T ValueCheck<T>(ref T value) where T : Object
    {
        if (value == null)
            value = FindObjectOfType<T>();

        return value;
    }
    
    
    public void OnBeforeSerialize(){}

    public void OnAfterDeserialize()
    {
        _gameData = _gameDataSerialize;
        //_personsData = _personsDataSerialize;
    }
}
