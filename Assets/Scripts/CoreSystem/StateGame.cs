﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Person
{
    public PersonSO personData { get; set; }
    public LeagueSO league { get; set; }

    public Person(){}

    public Person(PersonSO personData, LeagueSO league)
    {
        this.personData = personData;
        this.league = league;
    }
}

public static class StateGame
{
    private const string KEY_PLAYER = "Player";

    private static readonly LeaguesData _data;
    private static Person _player;
    private static Person _opponent;

    public static LeaguesData data => _data;
    public static Person player => _player;
    public static Person opponent => _opponent;
    
    public static bool isDeath { get; set; }

    static StateGame()
    {
        _data = Resources.Load("LeaguesData") as LeaguesData;
        SaveGame.LoadData();
        FixedFrameRater.SetTargetFrameRate(60);
    }

    public static string GetPlayerName()
    {
        return PlayerPrefs.GetString(KEY_PLAYER);
    }

    public static void SetPlayer(PersonSO player, LeagueSO league)
    {
        _player = new Person(player, league);
        PlayerPrefs.SetString(KEY_PLAYER, player.name);
    }

    public static void SetPlayer(Person player)
    {
        _player = player;
        PlayerPrefs.SetString(KEY_PLAYER, player.personData.name);
    }

    public static void SetOpponent(PersonSO opponent, LeagueSO league)
    {
        _opponent = new Person(opponent, league);
    }

    public static void Clear()
    {
        _player = null;
        _opponent = null;
        PlayerPrefs.SetString(KEY_PLAYER, "");
    }

    public static void RandomPersons()
    {
        _player = new Person();
        _player.league = _data.single;
        _player.personData = _player.league.GetRandomPerson();

        _opponent = new Person();
        _opponent.league = _data.tested;
        _opponent.personData = _opponent.league.GetRandomPerson();
    }

    public static void Lose()
    {
        Debug.Log("Lose");
        _player.personData.score = Score.lose;
        _opponent.personData.score = Score.win;
        isDeath = true;
    }
}