﻿using UnityEngine;
using UnityEngine.Events;

public class Money
{
    private static Money _instance;

    public static Money instance
    {
        get
        {
            if (_instance == null)
                _instance = new Money();

            return _instance;
        }
    }

    private const string KEY_MONEY = "Money";

    private int _money;
    public readonly UnityEvent onChangeMoney;

    public int getValue => _money;

    private Money()
    {
        this._money = PlayerPrefs.GetInt(KEY_MONEY);
        onChangeMoney = new UnityEvent();
    }

    public void Add(int value)
    {
        _money = Mathf.Clamp(_money + value, 0, int.MaxValue);
        Change();
    }

    public void Subtract(int value)
    {
        _money = Mathf.Clamp(_money - value, 0, int.MaxValue);
        Change();
    }

    public bool IncomeCheck(int verifiable)
    {
        return verifiable <= _money;
    }

    private void Change()
    {
        onChangeMoney.Invoke();
        PlayerPrefs.SetInt(KEY_MONEY, _money);
    }

    public void Clear()
    {
        _money = 0;
        Change();
    }
}
