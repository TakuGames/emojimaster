﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FixedFrameRater
{
    public static void SetTargetFrameRate(int countFrame)
    {
        Application.targetFrameRate = countFrame;
    }
}
