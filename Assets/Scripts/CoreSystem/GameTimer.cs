﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTimer : MonoBehaviour
{
    public int currentTime => Mathf.RoundToInt(_currentTime);

    private float _currentTime;
    private float _destinationTime;

    private bool _isCounting;

    private void Awake()
    {
        //_destinationTime = CoreSystem.gameData.gameSetup.endCountTime;
    }

    private void Start()
    {
        _isCounting = true;
    }

    public void CountingTime(bool value)
    {
        _isCounting = value;
    }

    private void Update()
    {
        if (!_isCounting)
            return;

        var startSpeed = CoreSystem.gameData.gameSetup.startSpeed;
        //var curveSpeed = CoreSystem.gameData.gameSetup.levelChangeGraph.Evaluate(_currentTime / _destinationTime);

        if (_currentTime < _destinationTime)
        {
            _currentTime += Time.deltaTime;
           // CoreSystem.gameData.gameSetup.currentLevelSpeed = startSpeed * curveSpeed;
        }
        else
        {
            _isCounting = false;
           // CoreSystem.gameData.reference.player.GetComponent<IDie>().Die(true);
           // Destroy(gameObject);
        }
    }
}
