﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class ScoreController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _scoreTargetTxt;
    [SerializeField] private ScoreGUI _playerScoreGUI;
    [SerializeField] private ScoreGUI _opponentScoreGUI;

    private int _scoreTarget;
    private PersonSO _player;
    private PersonSO _opponent;
    private int _curDiffScorePlayer;
    private int _curDiffScoreOpponent;
    

    public UnityEvent endSessionEvent = new UnityEvent();

    private void Start()
    {
        _player = StateGame.player.personData;
        _opponent = StateGame.opponent.personData;
        _scoreTarget = _opponent.power.scoreTarget;

        var playerStartLikes = _player.power.startLikes + _player.power.additionsStartLikes;
        var opponentStartLikes = _opponent.power.startLikes;
        
        _player.score = new Score(playerStartLikes, 0, playerStartLikes);
        _opponent.score = new Score(opponentStartLikes, 0, opponentStartLikes);
        
        _curDiffScorePlayer = _player.score.diffScore;
        _curDiffScoreOpponent = _opponent.score.diffScore;

        _scoreTargetTxt.text = _scoreTarget.ToString();
        _playerScoreGUI.Show(_curDiffScorePlayer);
        _opponentScoreGUI.Show(_curDiffScoreOpponent);
    }

    private void Update()
    {
        if (CheckScore(ref _curDiffScorePlayer, _player.score.diffScore, _playerScoreGUI) ||
            CheckScore(ref _curDiffScoreOpponent, _opponent.score.diffScore, _opponentScoreGUI))
            endSessionEvent.Invoke();
    }

    private bool CheckScore(ref int curScore, int score, ScoreGUI scoreGUI)
    {
        if (curScore != score)
        {
            curScore = score;
            scoreGUI.Show(curScore);
            if (curScore >= _scoreTarget)
                return true;
        }

        return false;
    }
}