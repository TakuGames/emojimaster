﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndSession : MonoBehaviour
{
    private void Start()
    {
        StateGame.isDeath = false;
    }

    public void End()
    {
        LoadScene.LoadSceneByIndex(2);
    }
}
