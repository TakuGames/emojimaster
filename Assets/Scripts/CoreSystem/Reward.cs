﻿
using UnityEngine;

public enum EPlayResult
{
    Win,
    Lose,
    WinLeague
}

public static class Reward
{
    private static int _reward;
    private static EPlayResult? _playResult = null;

    public static int reward
    {
        get
        {
            if (_reward == 0)
                DetermineReward();
            
            return _reward;
        }
    }

    public static EPlayResult result
    {
        get
        {
            if (_playResult == null)
                DetermineReward();
            
            return (EPlayResult)_playResult;
        }
    }

    public static void DetermineReward()
    {
        _reward = 0;
        _playResult = null;
        
        var opponent = StateGame.opponent;
        
        if (!opponent.personData.defeated)
        {
            _reward = opponent.personData.reward.lose;
            _playResult = EPlayResult.Lose;
            return;
        }

        if (opponent.league.completed)
        {
            _reward = opponent.league.reward;
            _playResult = EPlayResult.WinLeague;
        }
        else
        {
            _reward = opponent.personData.reward.win;
            _playResult = EPlayResult.Win;
        }
    }

    public static void Clear()
    {
        _reward = 0;
        _playResult = null;
    }
}