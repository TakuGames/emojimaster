﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[CreateAssetMenu(fileName = "GameData", menuName = "Dataset/GameData")]
public class GameDataSO : ScriptableObject, ISerializationCallbackReceiver
{
    [Serializable]
    public struct GameSetup
    {
        [Header("[Level Setup]")]

        [Range(0, 5)] public float startSpeed;

        public Ease levelEase;

        public float duration;
        public float indentY;
        public float indentX;
    }

    [Serializable]
    public struct ContentSetup
    {
        [Range(0, 100)] public int startCountUniqueMessage;
        [Range(0, 100)] public byte chanceMessage;
        [Range(0, 100)] public byte chanceStickers;
        [Range(0, 100)] public byte chanceUnique;
        [Range(0, 100)] public byte chanceForOpponent;
    }

    [Serializable]
    public struct MessageSetup
    {
        public byte minWidth;
        public byte maxWidth;
        public TextDataSO textData;

        public int minIndexGoodSmile;
        public int maxIndexGoodSmile;
        public int minIndexBadSmile;
        public int maxIndexBadSmile;
        
        [Range(0, 100)] public int chanceSmile;
    }
    
    [Serializable]
    public struct StickerSetup
    {
        public byte sizeRect;
        public StickersDataSO stickersData;
    }
    
    public class Reference
    {
        private PlayerController _player;

        public PlayerController player
        {
            get
            {
                if (_player == null)
                    _player = FindObjectOfType<PlayerController>();

                return _player;
            }
        }
    }

    public GameSetup gameSetup;
    public ContentSetup contentSetup;
    public MessageSetup messageSetup;
    public StickerSetup stickerSetup;

    public Reference reference = new Reference();
    
    private byte _curChanceMessage;
    private byte _curChanceStickers;

    public void OnBeforeSerialize()
    {
        _curChanceMessage = contentSetup.chanceMessage;
        _curChanceStickers = contentSetup.chanceStickers;
    }

    public void OnAfterDeserialize()
    {
        if (_curChanceMessage != contentSetup.chanceMessage)
        {
            _curChanceMessage = contentSetup.chanceMessage;
            contentSetup.chanceStickers = (byte) (100 - _curChanceMessage);
        }

        if (_curChanceStickers != contentSetup.chanceStickers)
        {
            _curChanceStickers = contentSetup.chanceStickers;
            contentSetup.chanceMessage = (byte) (100 - _curChanceStickers);
        }
    }

}