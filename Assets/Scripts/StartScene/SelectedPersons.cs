﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class SelectedPersons
{
    public PersonSO left;
    public PersonSO right;
    
    private static SelectedPersons instance;
 
    private SelectedPersons(){}
 
    public static SelectedPersons getInstance()
    {
        if (instance == null)
            instance = new SelectedPersons();
        return instance;
    }
}
