﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class StartController : MonoBehaviour
{
    [SerializeField] private PersonUI _player;
    [SerializeField] private PersonUI _opponent;
    [SerializeField] private TextMeshProUGUI _countOpponentsTxt;
    [SerializeField] private LeagueUI _league;

    private void Start()
    {
        LoadPlayer();
        AssignOpponent();
        AssignOpponentCount();

        _player.person = StateGame.player.personData;
        _opponent.person = StateGame.opponent.personData;
        _league.league = StateGame.opponent.league;
    }
    
    private void LoadPlayer()
    {
        string name = StateGame.GetPlayerName();
        var data = StateGame.data;
 
        LeagueSO selectLeague = data.single;
        PersonSO selectPlayer = string.IsNullOrEmpty(name) ? selectLeague.GetRandomPerson() : selectLeague.GetPerson(name);
 

        if (selectPlayer == null)
        {
            foreach (var league in data.leagues)
            {
                selectPlayer = league.GetPerson(name);
                if (selectPlayer != null)
                {
                    selectLeague = league;
                    break;   
                }
            }
        }

        if (selectPlayer == null)
            selectPlayer = data.single.GetRandomPerson();

        StateGame.SetPlayer(selectPlayer, selectLeague);
    }

    private void AssignOpponent()
    {
        LeagueSO league = null;
        PersonSO opponent = null;
        
        while (true)
        {
            league = StateGame.data.GetAccessibleLeague();
            if (league == null)
            {
                Debug.Log("End Game"); //TODO End game
                return;
            }

            opponent = league.GetOpponent();
            if (opponent == null)
            {
                league.completed = true;
                continue;
            }

            break;
        }
        
        StateGame.SetOpponent(opponent, league);
    }

    private void AssignOpponentCount()
    {
        _countOpponentsTxt.text = StateGame.opponent.league.GetNotDefeatedCount().ToString();
    }

    private void OnDestroy()
    {
        SaveGame.SaveData();
    }
}