﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMoveUp : MonoBehaviour
{
    [SerializeField] private int _speed;

    void Update()
    {
        transform.Translate(Vector2.up * _speed * Time.deltaTime);
    }
}
