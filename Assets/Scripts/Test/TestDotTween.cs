﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TestDotTween : MonoBehaviour
{
    [SerializeField] private Transform _someTransform;
    [SerializeField] private Ease _currentEase;
    [SerializeField] private float _duration;
    private void Start()
    {
        int a = 5;
        int b = 8;
        ref int pointer = ref a;
        pointer = 34;
        pointer = ref b;    // до версии 7.3 так делать было нельзя
        pointer = 6;

        //_someTransform.DOMoveX(2, 1);
        _someTransform.DOMove(new Vector3(2, 2, 2), _duration)
            .SetEase(_currentEase)
            .OnComplete(MyFunction);
    }

    private void MyFunction()
    {
        float x = Random.Range(-3, 3);
        float y = Random.Range(-3, 3);

        var randVector = new Vector2(x, y);
        _someTransform.DOMove(randVector, _duration)
            .SetEase(_currentEase)
            .OnComplete(MyFunction);
    }
}
