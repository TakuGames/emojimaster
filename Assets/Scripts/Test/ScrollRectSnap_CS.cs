﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScrollRectSnap_CS : MonoBehaviour
{
    // Public Variables
    [SerializeField] private RectTransform panel;   // To hold the ScrollPanel
    [SerializeField] private Button[] bttn;
    [SerializeField] private RectTransform center;	// Center to compare the distance for each button
    [SerializeField] private int _startButton = 1;

    // Private Variables
    public float[] distance;    // All buttons' distance to the center
    public float[] distReposition;
    private bool dragging = false;  // Will be true, while we drag the panel
    private int bttnDistance;   // Will hold the distance between the buttons
    private int minButtonNum;   // To hold the number of the button, with smallest distance to center
    private int bttnLength;

    void Start()
    {
        bttnLength = bttn.Length;
        distance = new float[bttnLength];
        distReposition = new float[bttnLength];

        // Get distance between buttons
        bttnDistance = (int)Mathf.Abs(bttn[1].GetComponent<RectTransform>().anchoredPosition.y - bttn[0].GetComponent<RectTransform>().anchoredPosition.y);

        panel.anchoredPosition = new Vector2((_startButton - 1) * 300, 0f);
    }

    void Update()
    {
        for (int i = 0; i < bttn.Length; i++)
        {
            distReposition[i] = center.GetComponent<RectTransform>().position.y - bttn[i].GetComponent<RectTransform>().position.y;
            distance[i] = Mathf.Abs(distReposition[i]);

            if (distReposition[i] > 2750)
            {
                float curX = bttn[i].GetComponent<RectTransform>().anchoredPosition.x;
                float curY = bttn[i].GetComponent<RectTransform>().anchoredPosition.y;

                Vector2 newAnchoredPos = new Vector2(curX, curY + (bttnLength * bttnDistance));
                bttn[i].GetComponent<RectTransform>().anchoredPosition = newAnchoredPos;
            }

            if (distReposition[i] < -2750)
            {
                float curX = bttn[i].GetComponent<RectTransform>().anchoredPosition.x;
                float curY = bttn[i].GetComponent<RectTransform>().anchoredPosition.y;

                Vector2 newAnchoredPos = new Vector2(curX, curY - (bttnLength * bttnDistance));
                bttn[i].GetComponent<RectTransform>().anchoredPosition = newAnchoredPos;
            }
        }

        float minDistance = Mathf.Min(distance);    // Get the min distance

        for (int a = 0; a < bttn.Length; a++)
        {
            if (minDistance == distance[a])
            {
                minButtonNum = a;
            }
        }

        if (!dragging)
        {
            //	LerpToBttn(minButtonNum * -bttnDistance);
            LerpToBttn(-bttn[minButtonNum].GetComponent<RectTransform>().anchoredPosition.y);
        }
    }

    void LerpToBttn(float position)
    {
        float newY = Mathf.Lerp(panel.anchoredPosition.x, position, Time.deltaTime * 5f);
        Vector2 newPosition = new Vector2(panel.anchoredPosition.x, newY);

        panel.anchoredPosition = newPosition;
    }

    public void StartDrag()
    {
        dragging = true;
    }

    public void EndDrag()
    {
        dragging = false;
    }

}













