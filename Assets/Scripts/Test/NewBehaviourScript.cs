﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.Scripting;
using UnityEngine.UI;
using Random = System.Random;

public class NewBehaviourScript : MonoBehaviour
{
    /// <summary>
    /// Инкапсуляция
    /// Наследование
    /// Полиморфизм
    /// Абстракция
    /// Посылка сообщений
    /// Повторное использование
    /// </summary>

    #region Generics

    public class Generics
    {
        /// <summary>
        /// Универасльные шаблоны
        /// </summary>

        public class MyGenericClass1<T> // параметризированный класс, T указатель места заполнения типом
        {
            private T field;
            private readonly string type;

            public MyGenericClass1()
            {
                this.type = field.GetType().ToString();
            }

            public void Method()
            {
                Debug.Log(type);
            }
        }

        public class MyGenericClass2<T1, T2>
        {
            private T1 _field1;
            private T2 _field2;

            public T1 field1
            {
                get => _field1;
                set => _field1 = value;
            }

            public T2 field2
            {
                get => _field2;
                set => _field2 = value;
            }

            public MyGenericClass2(T1 arg1, T2 arg2)
            {
                this._field1 = arg1;
                this._field2 = arg2;
            }
        }

        /// <summary>
        /// Универсальные методы
        /// </summary>

        public class MyClass
        {
            public void Method<T>(T argument)
            {
                T variable = argument;
                Debug.Log($"{variable.ToString()}   {variable.GetType()}");
            }
        }

        /// <summary>
        ///  Универсальные делегаты
        /// </summary>
        /// <param name="argument"> аргумент типа Т</param>
        /// <typeparam name="T"> передаваемый тип аргумента</typeparam>
        /// <typeparam name="TR"> возвращаемый тип аргумента</typeparam>
       
        public delegate TR MyDelegate<in T, out TR>(T argument); //suga
        //public delegate R MyDelegate<T, R>(T t);

        public delegate TR MyDelegateSum<in T1, in T2, out TR>(T1 arg1, T2 arg2);

        public class MyClassForDelegate
        {
            public int Add(int i)
            {
                return ++i;
            }

            public string Concatenation(string s)
            {
                return "Hello " + s;
            }

            public int Sum(int a, int b)
            {
                return a + b;
            }
            
            public float Sum(float a, float b)
            {
                return a + b;
            }
        }
        
        /// <summary>
        /// Ковариантность обобщений
        /// </summary>
        
        public abstract class Shape{}
        public class Circle : Shape{}

        public interface IContainer<T>
        {
            T figure { get; set; }
        }

        public interface IContainerGetCovar<out T> //out ключевое слово для ковариантности
        {
            T figure { get; }
        }
 
        public class Container<T> : IContainer<T>
        {
            public T figure { get; set; }

            public Container(T figure)
            {
                this.figure = figure;
            }
        }

        public class ContainerCovar<T> : IContainerGetCovar<T>
        {
            private T _figure;

            public ContainerCovar(T figure)
            {
                this._figure = figure;
            }

            public T figure
            {
                get { return _figure; }
            }
        }
        
        /// <summary>
        /// Контвариантность обобщений
        /// </summary>
        
        public interface IContainerSetContvar<in T> //in ключевое слово для контрвариантности
        {
            T figure { set; }
        }

        public class ContainerContrvar<T> : IContainerSetContvar<T>
        {
            private T _figure;

            public ContainerContrvar(T figure)
            {
                this._figure = figure;
            }

            public T figure
            {
                set { _figure = value; }
            }

            public override string ToString()
            {
                return _figure.GetType().ToString();
            }
            
        }
    }

    public static void TestGenerics()
    {
        {
            new Generics.MyGenericClass1<long>().Method(); //по слабой ссылке, в качестве параметра типа передаем тип int
        }
        {
            Generics.MyGenericClass2<int, float> instance2 = new Generics.MyGenericClass2<int, float>(2, 5.2f);
            Debug.Log($"{instance2.field1}  {instance2.field2}");
        }
        {
            new Generics.MyClass().Method("Hello");
            new Generics.MyClass().Method<int>(5); //suga
        }
        {
            Generics.MyDelegate<int, int> myDelegate1 = new Generics.MyClassForDelegate().Add;
            var result1 = myDelegate1(5);

            Generics.MyDelegate<string, string> myDelegate2 = new Generics.MyClassForDelegate().Concatenation;
            var result2 = myDelegate2("Alexander");

            Generics.MyDelegateSum<int, int, int> myDelegate3 = new Generics.MyClassForDelegate().Sum;
            var result3 = myDelegate3(3, 5);
            
            Generics.MyDelegateSum<float, float, float> myDelegate4 = new Generics.MyClassForDelegate().Sum;
            var result4 = myDelegate4(2.4f, 5.4f);

            Debug.Log($"{result1}   {result2}   {result3}   {result4}");
        }
        {
            Generics.Circle circle = new Generics.Circle();
            
            Generics.IContainer<Generics.Circle> container1 = new Generics.Container<Generics.Circle>(circle); //без ковариантности и контрвариантности
            Debug.Log(container1.figure.ToString());
            
            //ковариантность в обобщениях это upCast параметра типа
            Generics.IContainer<Generics.Shape> container2 = new Generics.Container<Generics.Shape>(circle); //неявный upCast (неявное привидение к базовому типу)
            Debug.Log(container2.figure.ToString());
            
            //Явная, настоящая ковариантность в обобщениях
            Generics.IContainerGetCovar<Generics.Shape> container3 = new Generics.ContainerCovar<Generics.Circle>(circle);  //UpCast параметра типа
            Debug.Log(container3.figure.ToString());
        }
        {
            Generics.Shape shape = new Generics.Circle(); //предварительный upCast
            
            Generics.IContainerSetContvar<Generics.Circle> container = new Generics.ContainerContrvar<Generics.Shape>(shape); //DownCast параметра типа
            Debug.Log(container.ToString());
        }
    }

    #endregion
    
    #region Delegates
  
    public static class Delegates01
    {
        /// <summary>
        /// объект, который может содержать в себе таблицу указателей на методы (технически)
        /// объект, который может содержать в себе набор методов сообщенных с делегатом
        /// </summary>
        public static class MyStaticClass
        {
            public static void Method()
            {
                Debug.Log("Static Method");
            }
        }

        public class MyClass
        {
            public void Method1()
            {
                Debug.Log("Method1");
            }

            public void Method2()
            {
                Debug.Log("Method2");
            }

            public void Method3()
            {
                Debug.Log("Method3");
            }

            public string MethodString(string text)
            {
                return "MethodString " + text;
            }
        }

        private delegate void MyDelegate();
        private delegate string MyDelegateString(string text); //типизированный делегат с возвращаемым значением типа string
        private delegate int MyDelegateSum(int a, int b);
        private delegate void MyDelegateRef(ref int a, ref int b, out int c);
        private delegate int MyDelegateLambda(int a);

        public static void TestDelegates()
        {
            MyDelegate delegate1 = new MyDelegate(MyStaticClass.Method); //передается метод объекта
            //MyDelegate delegate1 = MyStaticClass.Method; //предпорложение делегата
            delegate1.Invoke();
            delegate1();

            MyClass instance = new MyClass();
            delegate1 = instance.Method1;
            delegate1 += instance.Method2;
            delegate1();

            MyDelegateString delegateString = instance.MethodString;
            var text = delegateString("Invoke");
            Debug.Log(text);

            //комбинированные делегаты
            MyDelegate delegateCombine = null;
            MyDelegate d1 = instance.Method1;
            MyDelegate d2 = instance.Method2;
            MyDelegate d3 = instance.Method3;
            delegateCombine = d1 + d2 + d3;
            delegateCombine();

            delegateCombine -= d3; //разгрупировка
            delegateCombine();

            //анонимные делегаты
            MyDelegate anonimeDelegate = delegate { Debug.Log("AnonimeDelegate"); }; //сообщаем анонимный метод
            anonimeDelegate();

            MyDelegateSum delegateSum = delegate(int a, int b) { return a + b; }; //сообщаем анонимный метод с двумя аргументами и возвращаем значение результата метода
            Debug.Log($"sum = {delegateSum(2, 3)}");

            //с параметрами ref
            int num1 = 2;
            int num2 = 3;
            int sum;
            Debug.Log($"{num1} {num2}");
            MyDelegateRef delegateRef = delegate(ref int a, ref int b, out int c)
            {
                a++; b++; c = a + b;
            };
            delegateRef(ref num1, ref num2, out sum);
            Debug.Log($"{num1} + {num2} = {sum}");
            
            //лямбда выражения
            MyDelegateLambda delegateLambda1 = delegate(int x) { return x * 2; }; // Лямбда-Метод
            MyDelegateLambda delegateLambda2 = (x) => { return x * 2; }; // Лямбда-Оператор
            MyDelegateLambda delegateLambda3 = x => x * 2; // Лямбда-Выражение
            Debug.Log($"{delegateLambda1(2)}  {delegateLambda2(3)}  {delegateLambda3(4)}");

            MyDelegate delegateCombineLambda;
            string textSum = "";
            delegateCombineLambda = delegate { textSum += "a "; };
            delegateCombineLambda += () => { textSum += "b "; };
            delegateCombineLambda += () => Debug.Log(textSum);
            delegateCombineLambda();
        }
    }

    public static class Delegates02
    {
        /// <summary>
        /// Вложенные делегаты
        /// </summary>
        
        public delegate Delegate2 Delegate1();
        public delegate void Delegate2();
        
        public static Delegate2 Method1()
        {
            Debug.Log("Method1");
            return new Delegate2(Method2);
        }

        public static void Method2()
        {
            Debug.Log("Method2");
        }

        public static void TestDelegates()
        {
            Delegate1 delegate1 = Method1;
            Delegate2 delegate2 = delegate1();
            delegate2();
        }
    }

    public static class Delegates03
    {
        /// <summary>
        /// Вложенные делегаты
        /// </summary>
        
        public delegate Delegate3 FuncDelegate1(Delegate1 d1, Delegate2 d2);
        public delegate string Delegate1();
        public delegate string Delegate2();
        public delegate string Delegate3();

        public delegate Delegate1 FuncDelegate2(Delegate1 a, Delegate1 b);

        public static Delegate3 MethodFunc(Delegate1 d1, Delegate2 d2)
        {
            return delegate { return d1() + d2(); }; //sugar
            //return () => d1() + d2(); //лямбда выражение
        }

        public static string Method1() {return "Method1 ";}
        public static string Method2() {return "Method2 ";}

        public static void TestDelegates()
        {
            FuncDelegate1 funcDelegate1 = MethodFunc;
            Delegate3 delegate3 = funcDelegate1(Method1, Method2);
            Debug.Log(delegate3());

            Delegate1 delegateA = () => "Hello ";
            Delegate1 delegateB = () => "world!";
            FuncDelegate2 funcDelegate2 = delegate(Delegate1 a, Delegate1 b) { return delegate() { return a() + b(); }; }; //suga
            //FuncDelegate2 funcDelegate2 = (a, b) => () => a() + b(); //лямбда выражение
            Debug.Log(funcDelegate2(delegateA, delegateB)()); //func вызывает метод и возвращает делегат, который вызывает свой метод
            //Debug.Log(funcDelegate2.Invoke(delegateA, delegateB).Invoke()); //sugar
        }
    }

    public static class Delegates04
    {
        /// <summary>
        /// Рекурсия в лямбда операторах
        /// </summary>


        public delegate void MyDelegate(int value);

        public static void TestDelegates()
        {
            MyDelegate myDelegate = null;

            myDelegate = (int i) =>
            {
                i--;
                Debug.Log($"value begin: {i}");

                if (i > 0)
                {
                    myDelegate(i);
                }

                Debug.Log($"value end: {i}");
            };

            myDelegate(3);
        }
    }

    #endregion

    #region Struct

    /// <summary>
    /// могут наследоваться только от интерфейсов
    /// запрещено создавать конструктор по умолчанию
    /// может быть создан без конструктора (но не инициализирован)
    /// </summary>
    public interface IPrint
    {
        void Print();
    }

    struct MyStruct : IPrint
    {
        public int number;

        public MyStruct(int value)
        {
            this.number = value;
        }

        public void Print()
        {
            Debug.Log(number);
        }
    }

    public void TestStruct()
    {
        MyStruct test1 = new MyStruct(1); //пользовательский конструктор
        MyStruct test2 = new MyStruct(); //конструктор по умолчанию, опсывать не надо, запрещено создавать
        MyStruct test3; //конструктор не вызывается

        test1.Print();
        test2.Print();
        //test3.Print();

        test3.number = 3;
        test3.Print();
    }

    #endregion

    #region Class

    class MyClass : IPrint
    {
        public int number;

        public MyClass(int value)
        {
            this.number = value;
        }

        public MyClass() { }

        public void Print()
        {
            Debug.Log(number);
        }
    }

    public void TestClass()
    {
        MyClass test1 = new MyClass(1);
        MyClass test2 = new MyClass(); //конструктор по умлочнию, описать обязательно
        MyClass test3; //null

        test1.Print();
        test2.Print();
        //test3.Print();

        //test3.number = 3;
        //test3.Print();
    }

    #endregion

    #region Statica

    class NonStatic
    {
        public const float number = 2.5f;
        private static float _field;
        private int _id;

        public static float field
        {
            set => _field = value;
        }

        static NonStatic()
        {
            Debug.Log("Static constructor");
        }

        public NonStatic(int id)
        {
            Debug.Log("Non static constructor");
            this._id = id;
        }

        public void Method()
        {
            Debug.Log($"Instance{_id}.field = {_field}");
        }
    }

    private void TestStatic()
    {
        var inst1 = new NonStatic(1);
        var inst2 = new NonStatic(2);

        inst1.Method();
        inst2.Method();

        NonStatic.field = NonStatic.number;

        inst1.Method();
        inst2.Method();
    }

    #endregion

    #region Boxing

    /// <summary>
    /// запаковка типа значения в ссылочный тип
    /// братно UnBoxing обязательно явно указать тип DownCast
    /// может быть реализован upCast до типа object, до интерфейса, до абстракии ValueType
    /// </summary>
    public void TestBoxing()
    {
        Debug.Log("Test Boxing/Unboxing");

        MyStruct test = new MyStruct();
        object objTest = test;
        IPrint interfTest = test;
        ValueType valueTypeTest = test;

        MyStruct unTest1 = (MyStruct) objTest;
        MyStruct unTest2 = (MyStruct) interfTest;
        MyStruct unTest3 = (MyStruct) valueTypeTest;
    }

    #endregion

    #region Enum

    /// <summary>
    /// статический класс типа значений, хранящий константы, по умолчанию int
    /// может быть типизирован в любые целочисленные типы
    /// </summary>
    enum MyEnum : sbyte
    {
        Left = -1,
        Right = 1
    }

    enum EInt
    {
        Zero = 0,
        One,
        Two,
        Three,
        Four,
        Five
    }

    private void TestEnum()
    {
        int value = 5;
        Debug.Log(value * (sbyte) MyEnum.Left);
        Debug.Log(value * (sbyte) MyEnum.Right);

        MyEnum my = MyEnum.Left;
        Type enumObject = my.GetType(); //общая инфа об MyEnum
        Type instance = Enum.GetUnderlyingType(enumObject); //инфа об типе константы перечесления
        Debug.Log(
            $"{enumObject.Name}   {Enum.GetUnderlyingType(enumObject)}   {Enum.GetUnderlyingType(typeof(MyEnum))}");

        Type type = typeof(EInt); //typeof порождает экземпляр класса Type

        EInt digit = EInt.Four;
        Debug.Log($"HEX:  {Enum.Format(type, EInt.Two, "x")}");
        Debug.Log($"DEC:  {Enum.Format(type, digit, "D")}");
        Debug.Log($"STR:  {Enum.Format(type, 5, "G")}");

        //Находим элемент перечисления по имени и unbox к EInt
        EInt number = (EInt) Enum.Parse(typeof(EInt), "One");

        //определяем, является ли строка элементом перечисления
        bool flag = Enum.IsDefined(typeof(EInt), "Three");

        //в массив элементы перечисления
        Array array = Enum.GetValues(typeof(EInt));

        //вывод кол-ва элементов в перечислении
        Debug.Log($"Count enum: {array.Length}");

        //вывод всех элементов в перечислении
        for (int i = 0; i < array.Length; i++)
        {
            Debug.Log($"element {i}: {array.GetValue(i)}");
            int num = (int) array.GetValue(i); //не можем применять as, так как as может вернуть null
        }
    }

    #endregion

    #region Indexers

    public class Dictionary
    {
        private readonly string[] _keys;
        private readonly object[] _numbers;

        public object this[int index, string key]
        {
            set
            {
                _numbers[index] = value;
                _keys[index] = key;
            }
        }

        public object this[string key]
        {
            get
            {
                for (int i = 0; i < _keys.Length; i++)
                {
                    if (String.Equals(key, _keys[i]))
                        return _numbers[i];
                }

                return null;
            }
        }

        public object this[int index]
        {
            get
            {
                if (index >= 0 && index < _numbers.Length)
                    return _numbers[index];

                return null;
            }
        }

        public int length => _numbers.Length;

        public Dictionary(int length)
        {
            this._numbers = new object[length];
            this._keys = new string[length];
        }
    }

    #endregion


    private void Start()
    {
        Debug.Log(GC.GetTotalMemory(false));

        TestGenerics();
        
//        Delegates01.TestDelegates();
//        Delegates02.TestDelegates();
//        Delegates03.TestDelegates();
//        Delegates04.TestDelegates();

        //TestEnum();
        //  TestBoxing();

        // TestStruct();
        //  TestClass();

        // TestStatic();
    }


//    private void Start()
//    {
//        var numbers = new Dictionary(5);
//        for (int i = 0; i < numbers.length; i++)
//        {
//            numbers[i, i.ToString()] = $"число {i}";
//        }
//
//        for (int i = 0; i < numbers.length; i++)
//        {
//            Debug.Log(numbers[i.ToString()]);
//        }
//        
//        Debug.Log(numbers[9]);
//    }


//    private SelectedPersons _selected;
//    
//    private void Start()
//    {
//        _selected = SelectedPersons.getInstance();
//
//    }
//
//    public void Load()
//    {
//        //_selected.left = 
//        //    _selected.right = 
//        SceneManager.LoadSceneAsync("SampleScene");
//    }
}