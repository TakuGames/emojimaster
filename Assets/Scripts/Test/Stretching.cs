﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class Stretching : MonoBehaviour
{
    public UnityEvent OnFinished = new UnityEvent();

    [SerializeField] private PersonUI _avatar;
    [SerializeField] private TextMeshProUGUI _resultTMP;

    [SerializeField] private RectTransform _likeRectTransform;
    [SerializeField] private RectTransform _dislikeRectTransform;

    [SerializeField] private AnimationCurve _curve;

    [SerializeField] private float _speed;
    [SerializeField] private float _heightMax;

    [SerializeField] private bool _left;

    private float _lerpTime;
    private float _likeHeight;
    private float _disLikeHeight;
    private float _oneLikeHeight;

    private IEnumerator Scalling()
    {
        WaitForEndOfFrame frame = new WaitForEndOfFrame();

        while (_lerpTime < 1)
        {
            yield return frame;
            ChangeFiller(_likeHeight, _likeRectTransform.sizeDelta, _likeRectTransform);

            _resultTMP.text = $"LIKES: { Mathf.RoundToInt(_likeRectTransform.sizeDelta.y / _oneLikeHeight)}";
        }
        _lerpTime = 0;

        _resultTMP.color = Color.red;
        while (_lerpTime < 1)
        {
            yield return frame;
            ChangeFiller(_disLikeHeight, _dislikeRectTransform.sizeDelta, _dislikeRectTransform);

            _resultTMP.text = $"DISLIKES: { Mathf.RoundToInt(_dislikeRectTransform.sizeDelta.y / _oneLikeHeight)}";
        }
        _lerpTime = 0;

        // change dislike pivot position Y
        var pivotDislike = _dislikeRectTransform.pivot;
        pivotDislike.y = 0;
        _dislikeRectTransform.pivot = pivotDislike;

        // set same  local position
        var pos = _dislikeRectTransform.localPosition;
        pos.y -= _dislikeRectTransform.rect.height;
        _dislikeRectTransform.localPosition = pos;

        // set to next parent
        _dislikeRectTransform.parent = transform.parent;
        _dislikeRectTransform.SetAsLastSibling();

        // calculate final height
        var finalHeight = _likeRectTransform.rect.height - _dislikeRectTransform.rect.height;
        var dislikeSizeDelta = _dislikeRectTransform.sizeDelta;

        _resultTMP.color = Color.white;

        while (_lerpTime < 1)
        {
            yield return frame;

            dislikeSizeDelta.y = _likeRectTransform.sizeDelta.y - finalHeight;

            ChangeFiller(finalHeight, _likeRectTransform.sizeDelta, _likeRectTransform);
            ChangeFiller(dislikeSizeDelta, _dislikeRectTransform);

            _resultTMP.text = $"LIKES: { Mathf.RoundToInt(_likeRectTransform.sizeDelta.y / _oneLikeHeight)}";
        }

        _lerpTime = 0;

        _dislikeRectTransform.parent = transform;
        _likeRectTransform.parent = transform;

        _dislikeRectTransform.SetAsLastSibling();
        _likeRectTransform.SetAsLastSibling();

        OnFinished.Invoke();
    }

    private void Start()
    {
        SelectedPersons selector = SelectedPersons.getInstance();

        _avatar.person = (_left) ? selector.left : selector.right;

        // test
        //selector.left = CoreSystem.personsData.GetPerson(0);
        //selector.right = CoreSystem.personsData.GetPerson(1);

        var leftLikes = selector.left.score.like;
        var leftDislikes = selector.left.score.dislike;

        var rightLikes = selector.right.score.like;
        var rightDislikes = selector.right.score.dislike;

       // leftLikes = 1501;
       // leftDislikes = 0;
       //
       // rightLikes = 1;
       // rightDislikes = 15;

        float maxLikes = (leftLikes > rightLikes) ? leftLikes : rightLikes;
        _oneLikeHeight = _heightMax / maxLikes;

        _likeHeight = (_left) ? _oneLikeHeight * leftLikes : _oneLikeHeight * rightLikes;
        _disLikeHeight = (_left) ? _oneLikeHeight * leftDislikes : _oneLikeHeight * rightDislikes;

        if (_disLikeHeight > _likeHeight)
            _disLikeHeight = _likeHeight;

        StartCoroutine(Scalling());
    }

    private void ChangeFiller(float destinationHeight, Vector2 size, RectTransform currentTransform)
    {
        _lerpTime += Time.deltaTime * _speed;
        size.y = Mathf.Lerp(size.y, destinationHeight, _curve.Evaluate(_lerpTime));

        if (currentTransform)
            currentTransform.sizeDelta = size;
    }
    private void ChangeFiller(Vector2 finalSize, RectTransform currentRect)
    {
        currentRect.sizeDelta = finalSize;
    }
}
