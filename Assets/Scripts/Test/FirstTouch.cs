﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstTouch : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
            ActivatePause();
    }
    private void ActivatePause()
    {
        var pause = FindObjectOfType<PauseController>();

        if (!pause)
            return;

        pause.enabled = true;

        Destroy(this);
    }
}
