﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MyDebug : MonoBehaviour
{
    private static MyDebug _istance;
    public static MyDebug instance
    {
        get
        {
            if(_istance == null)
                _istance = (MyDebug) FindObjectOfType(typeof(MyDebug));
 
            return _istance;
        }
    }

    [SerializeField] private TextMeshProUGUI _text;

    public void Debuging<T>(T obj)
    {
        var text = obj.ToString();
        _text.text += text + '\n';
        Debug.Log(obj);
    }
}
