﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneBtn : MonoBehaviour
{
    [SerializeField] private int _sceneNumber;

    public void OnBtn()
    {
        LoadScene.LoadSceneByIndex(_sceneNumber);
    }

}
