﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class TransitionLoadScene : MonoBehaviour
{
    [SerializeField] [Range(0f, 1f)] private float _blackout = 0.1f; 
    [SerializeField] private Image[] _images;
    
    private void Awake()
    {
        var canvas = GetComponent<Canvas>();
        canvas.worldCamera = Camera.main;
    }

    private void Start()
    {
        var opponent = StateGame.opponent;
        if (opponent == null)
            return;
        
        var color = opponent.league.color;

        color.r = Mathf.Clamp(color.r - _blackout, 0f, 1f);
        color.g = Mathf.Clamp(color.g - _blackout, 0f, 1f);
        color.b = Mathf.Clamp(color.b - _blackout, 0f, 1f);
        
        foreach (var image in _images)
        {
            image.color = color;
        }
    }
}
