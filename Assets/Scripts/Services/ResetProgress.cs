﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResetProgress : MonoBehaviour
{
    public void OnReset()
    {
        var data = StateGame.data;

        foreach (var league in data.leagues)
        {
            league.completed = false;

            foreach (var person in league.persons)
            {
                person.price.acquired = false;
                person.price.advertising = 3;
                person.defeated = false;
                person.power.additionsStartLikes = 0;
            }

            league.persons[league.personsCount - 1].price.advertising = 5;
        }

        foreach (var person in data.single.persons)
        {
            person.price.acquired = true;
            person.power.additionsStartLikes = 0;
        }
        
        PlayerPrefs.SetString("Player", "");
        
        Money.instance.Clear();
        SaveGame.SaveData();
        SceneManager.LoadScene(0);
    }
}