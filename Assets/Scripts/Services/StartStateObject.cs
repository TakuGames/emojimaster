﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartStateObject : MonoBehaviour
{
    public enum EStartState
    {
        Destroy,
        Disable,
    }

    [SerializeField] private EStartState _state;
    // Start is called before the first frame update
    void Awake()
    {
        switch (_state)
        {
            case EStartState.Destroy:
                Destroy(gameObject);
                break;
            case EStartState.Disable:
                gameObject.SetActive(false);
                Destroy(this);
                break;
        }
    }
}
