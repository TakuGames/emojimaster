﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PutInPlace : MonoBehaviour
{
    public enum ETypePut
    {
        Top,
        Bottom
    }

    [SerializeField] private ETypePut _typePut;
    [SerializeField] private bool _isRemoveCollider = false;

    private BoxCollider2D _collider;

    // Start is called before the first frame update
    void Start()
    {
        _collider = GetComponent<BoxCollider2D>();
        if (_collider == null)
            Remove();

        var height = _collider.size.y * transform.localScale.y;
        var offset = _collider.offset.y * transform.localScale.y;

        float newY = 0f;
        switch (_typePut)
        {
            case ETypePut.Top:
                newY = Camera.main.orthographicSize + Camera.main.transform.position.y - height * 0.5f - offset;
                break;
            case ETypePut.Bottom:
                newY = -Camera.main.orthographicSize + Camera.main.transform.position.y + height * 0.5f - offset;
                break;
        }

        var newPos = transform.position;
        newPos.y = newY;
        transform.position = newPos;
        Remove();
    }

    private void Remove()
    {
        if (_isRemoveCollider && _collider != null)
            Destroy(_collider);
        
        Destroy(this);
    }
}