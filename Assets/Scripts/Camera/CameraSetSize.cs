﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSetSize : MonoBehaviour
{
    [SerializeField] private Transform _background;
    private float _wightBackground;
    private float _hightBackground;

    private void Awake()
    {
        var spriteBack = _background.GetComponent<SpriteRenderer>();

        if (spriteBack)
        {
            _wightBackground = spriteBack.bounds.size.x;
            _hightBackground = spriteBack.bounds.size.y;

            var camera = Camera.main;
            
            camera.orthographicSize = _wightBackground / (camera.aspect * 2);

            var newPosCam = Vector3.zero;
            newPosCam.y = (_hightBackground * 0.5f) - camera.orthographicSize;
            newPosCam.z = -100f;
            camera.transform.position = newPosCam;
        }
    }
}
