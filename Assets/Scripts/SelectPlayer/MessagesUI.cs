﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MessagesUI : MonoBehaviour
{
    [SerializeField] private GameObject _like;
    [SerializeField] private GameObject _dislike;
    private TextMeshProUGUI _likeTxt;
    private TextMeshProUGUI _dislikeTxt;
    private ESent _sent = ESent.Good;
    private WaitForSeconds _showDelay;
    private PersonSO _person;
    private bool _leagueCompleted;

    public void SetPerson(Person selectedPerson)
    {
        _person = selectedPerson.personData;
        _leagueCompleted = selectedPerson.league.completed;
        _like.SetActive(false);
        _dislike.SetActive(false);
    }

    private void Awake()
    {
        _likeTxt = _like.GetComponentInChildren<TextMeshProUGUI>();
        _dislikeTxt = _dislike.GetComponentInChildren<TextMeshProUGUI>();
    }

    private void Start()
    {
        _like.SetActive(false);
        _dislike.SetActive(false);
        _showDelay = new WaitForSeconds(3);
        StartCoroutine(ShowMessages());
    }


    private IEnumerator ShowMessages()
    {
        string text;
        
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(5, 10));

            if (_person == null || _leagueCompleted == false)
                continue;

            text = _person.GetRandomReplicaPerson(_sent);
            
            switch (_sent)
            {
                case ESent.Good:
                    _sent = ESent.Bad;
                    _like.SetActive(true);
                    _likeTxt.text = text;
                    break;
                case ESent.Bad:
                    _sent = ESent.Good;
                    _dislike.SetActive(true);
                    _dislikeTxt.text = text;
                    break;
            }

            yield return _showDelay;
            
            _like.SetActive(false);
            _dislike.SetActive(false);
        }
    }
}
