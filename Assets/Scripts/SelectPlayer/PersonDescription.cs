﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PersonDescription : MonoBehaviour
{
    [SerializeField] private Image _backgroundName;
    [SerializeField] private TextMeshProUGUI _name;
    [SerializeField] private StartLikes _startLikes;

    
    public void SetPerson(PersonSO person, LeagueSO league)
    {
        _backgroundName.color = person.avatar.color;
        _name.text = person.name;

        if (!league.completed)
        {
            _startLikes.gameObject.SetActive(false);
            return;
        }
        
        _startLikes.gameObject.SetActive(true);
        _startLikes.Assign(person);
    }
}
