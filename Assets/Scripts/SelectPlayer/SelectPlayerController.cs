﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectPlayerController : MonoBehaviour
{
    [Header("Descriptions")] [SerializeField]
    private LeagueUI _leagueDescription;

    [SerializeField] private PersonDescription _personDescription;
    [SerializeField] private PersonButtons _personButtons;
    [SerializeField] private MessagesUI _messages;
    private ScrollersController _scrollersController;
    private Person _selectedPerson;
    public Person selectedPerson => _selectedPerson;

    private void Awake()
    {
        _scrollersController = GetComponent<ScrollersController>();
    }

    private void Start()
    {
        //TODO tested
        //Loading random player 
        var league = StateGame.data.tested;
        var person = league.GetRandomPerson();
        _selectedPerson = new Person(person, league);

        //Loading saved player 
        //_selectedPerson = StateGame.player;


        AssignLeague(_selectedPerson.league);
        AssignPerson(_selectedPerson.personData);


        _scrollersController.OnChangeLeague.AddListener(AssignLeague);
        _scrollersController.OnChangePerson.AddListener(AssignPerson);
    }

    //called from parameterized Event selected League
    public void AssignLeague(LeagueSO league)
    {
        _selectedPerson.league = league;
        _leagueDescription.league = league;
        //TODO assign to leagueSlider
    }

    //called from parameterized Event selected Person
    public void AssignPerson(PersonSO person)
    {
        _selectedPerson.personData = person;
        _personDescription.SetPerson(person , _selectedPerson.league);
        _personButtons.SetPerson(_selectedPerson);
        _messages.SetPerson(_selectedPerson);
        //TODO assign to playerSlider
    }

    private void OnDestroy()
    {
        _scrollersController.OnChangeLeague.RemoveListener(AssignLeague);
        _scrollersController.OnChangePerson.RemoveListener(AssignPerson);
    }
}