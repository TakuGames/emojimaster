﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BuyPlayerBtn : ASelectPlayerBtn
{
    [SerializeField] private Color _colorEnable;
    [SerializeField] private Color _colorDisable;
    private Image _image;
    private Button _button;
    private TextMeshProUGUI _price;
    private Money _money;
    private PersonButtons _personButtons;

    private void Awake()
    {
        _money = Money.instance;
        _image = GetComponent<Image>();
        _button = GetComponent<Button>();
        _price = GetComponentInChildren<TextMeshProUGUI>();
        _personButtons = GetComponentInParent<PersonButtons>();
    }

    private void Start()
    {
        _money.onChangeMoney.AddListener(Refresh);
    }

    public override void Show(PersonSO personData)
    {
        base.Show(personData);
        _price.text = _personData.price.coins.ToString();
        Refresh();
    }

    private void Refresh()
    {
        if (_money.getValue >= _personData.price.coins)
        {
            _image.color = _colorEnable;
            _button.enabled = true;
        }
        else
        {
            _image.color = _colorDisable;
            _button.enabled = false;
        }
    }

    public void OnBuy()
    {
        _personData.price.acquired = true;
        _money.Subtract(_personData.price.coins);
        _personButtons.Refresh();
    }
}
