﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AdvPlayerBtn : ASelectPlayerBtn
{
    private TextMeshProUGUI _price;

    private void Awake()
    {
        _price = GetComponentInChildren<TextMeshProUGUI>();
    }

    public override void Show(PersonSO personData)
    {
        base.Show(personData);
        _price.text = $"x {personData.price.advertising}";
    }
}
