﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonButtons : MonoBehaviour
{
    private SelectPlayerBtn _selectBtn;
    private BuyPlayerBtn _buyBtn;
    private AdvPlayerBtn _advBtn;
    private PersonSO _personData;

    private void Awake()
    {
        _selectBtn = GetComponentInChildren<SelectPlayerBtn>(true);
        _buyBtn = GetComponentInChildren<BuyPlayerBtn>(true);
        _advBtn = GetComponentInChildren<AdvPlayerBtn>(true);
    }

    public void SetPerson(Person selectedPerson)
    {
        _personData = selectedPerson.personData;
        DisableAllBtns();
        
        if (!selectedPerson.league.completed)
            return;
        
        ShowBtns(selectedPerson.personData);
    }

    private void DisableAllBtns()
    {
        _selectBtn.Hide();
        _buyBtn.Hide();
        _advBtn.Hide();
    }

    private void ShowBtns(PersonSO personData)
    {
        if (personData.price.acquired)
        {
            _selectBtn.Show();
            return;
        }
        
        _buyBtn.Show(personData);
        _advBtn.Show(personData);
    }
    
    public void Refresh()
    {
        DisableAllBtns();
        
        if (_personData.price.acquired)
        {
            _selectBtn.Show();
            return;
        }
        
        _buyBtn.Show(_personData);
        _advBtn.Show(_personData);
    }
    
    
}
