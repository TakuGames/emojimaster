﻿using UnityEngine;
using System;
using DG.Tweening;

public class VFXEffect : MonoBehaviour
{
    [SerializeField] private string name;

    private VFXPull _fXPull;
    public VFXPull fXPull
    {
        set => _fXPull = value;
    }

    private Vector3 _startScale;

    private void Deactivate()
    {
        gameObject.SetActive(false);
        transform.localScale = _startScale;
        _fXPull.ReturnFX(this);
    }

    public void Activating()
    {
        _startScale = transform.localScale;

        gameObject.SetActive(true);

        var sequense = DOTween.Sequence();
        sequense
            .Join(transform.DOMove(Vector2.up * 3, 1).SetEase(Ease.Flash))
            .Join(transform.DOScale(Vector2.zero, 1).SetEase(Ease.Flash));

        sequense.OnComplete(Deactivate);
    }
    public void Activating(Vector3 finalPos)
    {
        _startScale = transform.localScale;

        gameObject.SetActive(true);

        var sequense = DOTween.Sequence();

        sequense
            .Join(transform.DOJump(finalPos, 2, 1, 0.8f).SetEase(Ease.Flash))
            .Join(transform.DOScale(Vector2.zero, 0.8f).SetEase(Ease.Flash));

        sequense.Play();
        sequense.OnComplete(Deactivate);
    }
}
