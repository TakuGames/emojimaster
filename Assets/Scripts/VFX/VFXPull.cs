﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXPull
{
    private GameObject _effectsPrefab;
    private Stack<VFXEffect> _effects;

    public VFXPull(GameObject obj)
    {        
        _effectsPrefab = obj;
        _effects = new Stack<VFXEffect>();        
    }
    public VFXEffect GetEffect()
    {
        VFXEffect currentEffect;
        
        if (_effects.Count > 0)
        {
            currentEffect = _effects.Pop();
        }
        else
        {
            currentEffect = CoreSystem.Instantiate(_effectsPrefab).GetComponent<VFXEffect>();
        }

        currentEffect.fXPull = this;
        return currentEffect;
    }

    public void ReturnFX(VFXEffect fx)
    {
        _effects.Push(fx);
    }
}
