﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class VFXController : MonoBehaviour
{
    [SerializeField] private Transform _player;
    [SerializeField] private Transform _opponent;

    [SerializeField] private GameObject[] _effectPrefabs;

    private Dictionary<string, VFXPull> _pulls = new Dictionary<string, VFXPull>();

    private void Awake()
    {
        for (int i = 0; i < _effectPrefabs.Length; i++)
        {
            var pull = new VFXPull(_effectPrefabs[i]);
            _pulls.Add(_effectPrefabs[i].name, pull);
        }
    }

    public VFXEffect GetFX(string name)
    {
        if (!_pulls.ContainsKey(name))
        {
            Debug.Log("No this effect");
            return null;
        }
        return _pulls[name].GetEffect();
    }

    public void PlayEffectOnes(string effectName, Vector2 startPos)
    {
        var effect = GetFX(effectName);

        if (!effect) return;
        if (startPos.sqrMagnitude <= 0) return;

        effect.transform.parent = transform;
        effect.transform.position = startPos;
        effect.Activating();
    }
    public void PlayEffectOnes(string effectName, string personName, Vector2 startPos)
    {
        var effect = GetFX(effectName);

        if (!effect) return;
        if (personName == null) return;
        if (startPos.sqrMagnitude <= 0) return;

        var finalPos = (personName != StateGame.player.personData.name) ? _opponent.position : _player.position;

        effect.transform.parent = transform;
        effect.transform.position = startPos;
        effect.Activating(finalPos);
    }
}

