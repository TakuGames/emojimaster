﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Events;

public class DetermineWinner : MonoBehaviour
{
    public UnityEvent onDetermineWinner = new UnityEvent();
    
    
    private void Start()
    {
        Debug.Log("Determine");
        
        if (StateGame.player.personData.score > StateGame.opponent.personData.score)
            WinOpponent();
        else
            LoseLeague();

        SaveGame.SaveData();
        Reward.DetermineReward();
        
        onDetermineWinner.Invoke();
    }

    private void WinOpponent()
    {
        StateGame.opponent.personData.defeated = true;
        CheckWinLeague();
    }

    private void CheckWinLeague()
    {
        foreach (var person in StateGame.opponent.league.persons)
        {
            if (!person.defeated)
                return;
        }

        StateGame.opponent.league.completed = true;
    }

    private void LoseLeague()
    {
        foreach (var person in StateGame.opponent.league.persons)
        {
            person.defeated = false;
        }
    }
}
