﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ResultGameUI : MonoBehaviour
{
    [Header("Effects")]
    [SerializeField] private GameObject _beams;

    [Header("Elements")]
    [SerializeField] private GameObject _lose;
    [SerializeField] private GameObject _win;
    [SerializeField] private GameObject _winLeague;

    [Header("Reward Count")]
    [SerializeField] private GameObject _reward;

    private TextMeshProUGUI _rewardTxt;

    private void Awake()
    {
        _rewardTxt = _reward.GetComponentInChildren<TextMeshProUGUI>();
    }

    private void Start()
    {
        _beams.SetActive(false);
        _lose.SetActive(false);
        _win.SetActive(false);
        _winLeague.SetActive(false);
        
        _reward.SetActive(false);
    }

    public void Show()
    {
        switch (Reward.result)
        {
            case EPlayResult.Lose:
                _lose.SetActive(true);
                Debug.Log("Lose activate");
                break;
            case EPlayResult.Win:
                _win.SetActive(true);
                _beams.SetActive(true);
                break;
            case EPlayResult.WinLeague:
                _winLeague.SetActive(true);
                _beams.SetActive(true);
                break;
        }
        
        _reward.SetActive(true);
        _rewardTxt.text = Reward.reward.ToString();
    }
}