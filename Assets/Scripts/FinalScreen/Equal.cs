﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Equal : AStateOpponentFinish
{
    protected override void Animate()
    {
        Sequence seq = DOTween.Sequence();
        seq.Append(_rect.DOMoveY(0f, 1f));
        seq.Play();
    }
}
