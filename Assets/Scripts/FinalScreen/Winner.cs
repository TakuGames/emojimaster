﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Winner : AStateOpponentFinish
{
    protected override void Animate()
    {
        Sequence seq = DOTween.Sequence();
        seq.Append(_rect.DOScale(new Vector3(1.5f, 1.5f, 1f), 1f));
        seq.AppendCallback((() => _animator.SetTrigger("Winner")));
        seq.Play();
    }
}
