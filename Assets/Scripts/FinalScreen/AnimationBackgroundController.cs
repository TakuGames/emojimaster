﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class AnimationBackgroundController : MonoBehaviour
{
    [SerializeField] private GameObject _win;
    [SerializeField] private GameObject _lose;
    
    [Header("Fade Panel")]
    [SerializeField] private Image _panelImg;
    [SerializeField] private float _duration = 5f;

    private void Start()
    {
        StartPanel();
        Play();
    }

    private void Play()
    {
        var result = Reward.result != EPlayResult.Lose;

        _win.SetActive(result);
        _lose.SetActive(!result);
    }

    private void StartPanel()
    {
        if (_panelImg == null)
            return;
        
        var color = _panelImg.color;
        color.a = 1f;
        _panelImg.color = color;
    }

    public void Show()
    {
        if (_panelImg == null)
            return;
        
        _panelImg.DOFade(0f, _duration);
    }
}
