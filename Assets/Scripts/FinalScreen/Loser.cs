﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Loser : AStateOpponentFinish
{
    protected override void Animate()
    {
        Sequence seq = DOTween.Sequence();
        seq.Append(_rect.DOScale(new Vector3(1.2f, 1.2f, 1f), 1f));
        seq.AppendCallback((() => _animator.SetTrigger("Loser")));
        seq.Play();
    }
}
