﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class StretchingController : MonoBehaviour
{
    public struct ScoreHeight
    {
        public float like;
        public float dislike;

        public ScoreHeight(Score diffScore, float oneHeight)
        {
            this.like = oneHeight * diffScore.like;
            this.dislike = diffScore.dislike > diffScore.like ? like : oneHeight * diffScore.dislike;
        }
    }


    [Serializable]
    public class Opponent
    {
        [SerializeField] private GameObject _personGO;
        [SerializeField] private PersonUI _personUi;
        [SerializeField] private RectTransform _likeRect;
        [SerializeField] private RectTransform _dislikeRect;
        private RectTransform _rectTransform;
        public PersonUI personUi => _personUi;
        public GameObject personGO => _personGO;

        public Score diffScore
        {
            get { return _personUi.person.score; }
            set { _personUi.person.score = value; }
        }

        public ScoreHeight scoreHeight { get; set; }
        public AStateOpponentFinish stateFinish { get; set; }
        public bool isShowAddLikeBtn { get; set; }

        public RectTransform likeRect => _likeRect;
        public RectTransform dislikeRect => _dislikeRect;

        public RectTransform rectTransform
        {
            get
            {
                if (_rectTransform == null)
                    _rectTransform = _personUi.GetComponent<RectTransform>();
                return _rectTransform;
            }
        }

        public void ShowState()
        {
            stateFinish.enabled = true;
        }
    }

    [SerializeField] private Opponent _player;
    [SerializeField] private Opponent _opponent;
    [SerializeField] private float _heightMax;
    public Opponent player => _player;
    public Opponent opponent => _opponent;

    [Header("Timings")] [SerializeField] private float _duration = 1f;
    [SerializeField] private float _interval = 0.5f;

    public UnityEvent OnFinishedDrawResult = new UnityEvent();

    [Header("Testing")] [SerializeField] private bool _isEqualResult = false;


    private void Start()
    {
    #if !UNITY_EDITOR
            _isEqualResult = false;
    #endif
        
        _player.personGO.SetActive(true);
        _opponent.personGO.SetActive(true);

        if (StateGame.player == null || StateGame.opponent == null)
        {
            StateGame.RandomPersons();
        }
        
        _player.personUi.person = StateGame.player.personData;
        _opponent.personUi.person = StateGame.opponent.personData;

        var playerDiffScore = _player.diffScore;
        var opponentDiffScore = _opponent.diffScore;

        float maxLikes = (playerDiffScore.like > opponentDiffScore.like)
            ? playerDiffScore.like
            : opponentDiffScore.like;
        var oneLikeHeight = _heightMax / maxLikes;

        _player.scoreHeight = new ScoreHeight(playerDiffScore, oneLikeHeight);
        _opponent.scoreHeight = new ScoreHeight(opponentDiffScore, oneLikeHeight);

        //Debug.Log($"{_leftOpponent.scoreHeight.like}:{_leftOpponent.scoreHeight.dislike}   {_rightOpponent.scoreHeight.like}:{_rightOpponent.scoreHeight.dislike}");

        SetStatesOpponents();
        DrawResult();
    }

    private void DrawResult()
    {
        Sequence seq = DOTween.Sequence();
        seq.AppendInterval(_interval);
        seq.Join(AnimateStretching(_player));
        seq.Join(AnimateStretching(_opponent));

        seq.AppendInterval(_interval);
        seq.AppendCallback(() => OnFinishedDrawResult.Invoke());
        seq.Play();
    }

    private Sequence AnimateStretching(Opponent opponent)
    {
        var scoreHeight = opponent.scoreHeight;

        Sequence seq = DOTween.Sequence();

        Debug.Log(StateGame.isDeath);
        
        if (!StateGame.isDeath)
        {
            //draw like
            seq.Append(opponent.likeRect.DOSizeDelta(new Vector2(300, scoreHeight.like), _duration));
            seq.AppendInterval(_interval);

            if (scoreHeight.dislike > 0)
            {
                //draw dislike
                seq.Append(opponent.dislikeRect.DOSizeDelta(new Vector2(300, scoreHeight.dislike), _duration));
                seq.AppendInterval(_interval);

                //draw difference
                seq.Append(opponent.likeRect.DOSizeDelta(new Vector2(300, scoreHeight.like - scoreHeight.dislike),
                    _duration));
                seq.Join(opponent.dislikeRect.DOSizeDelta(new Vector2(300, 0), _duration));
            }
            
            //draw states
            seq.AppendInterval(_interval);
        }

        seq.AppendCallback(() =>
        {
            AssignPosition(opponent.rectTransform);
            opponent.ShowState();
        });
        seq.Join(opponent.likeRect.DOSizeDelta(new Vector2(300, 0), _duration));
        seq.Join(opponent.rectTransform.DOMoveY(0f, 1f));

        seq.AppendInterval(_interval);
        return seq;
    }

    private void AssignPosition(RectTransform rect)
    {
        rect.parent = gameObject.transform;

        var position = rect.localPosition;

        var halfVector = new Vector2(0.5f, 0.5f);
        rect.pivot = halfVector;
        rect.anchorMax = halfVector;
        rect.anchorMin = halfVector;

        position.y += rect.sizeDelta.y * 0.5f;
        rect.localPosition = position;
    }

    private void SetStatesOpponents()
    {
        var leftOpponentResult = Mathf.Clamp(_player.diffScore.like - _player.diffScore.dislike, 0, int.MaxValue);
        var rightOpponentResult = Mathf.Clamp(_opponent.diffScore.like - _opponent.diffScore.dislike, 0, int.MaxValue);

        _player.stateFinish =
            (AStateOpponentFinish) _player.personUi.gameObject.AddComponent(
                leftOpponentResult > rightOpponentResult ? typeof(Winner) : typeof(Loser));
        _opponent.stateFinish =
            (AStateOpponentFinish) _opponent.personUi.gameObject.AddComponent(
                rightOpponentResult > leftOpponentResult ? typeof(Winner) : typeof(Loser));

        _player.stateFinish.enabled = false;
        _opponent.stateFinish.enabled = false;

        _player.isShowAddLikeBtn = leftOpponentResult == rightOpponentResult;
        _opponent.isShowAddLikeBtn = leftOpponentResult == rightOpponentResult;
    }
}