﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AStateOpponentFinish : MonoBehaviour
{
    protected RectTransform _rect;
    protected Animator _animator;

    private void Awake()
    {
        _rect = GetComponent<RectTransform>();
        _animator = GetComponent<Animator>();
    }

    private void Start()
    {
        Animate();
    }

    protected abstract void Animate();
}
