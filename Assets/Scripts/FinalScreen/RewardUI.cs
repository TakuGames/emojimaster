﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RewardUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _descriptionTxt;
    [SerializeField] private TextMeshProUGUI _valueRewardTxt; 
    
    private void Start()
    {
        gameObject.SetActive(false);
    }

    public void ShowReward(int valueReward)
    {
        gameObject.SetActive(true);
        _valueRewardTxt.text = valueReward.ToString();
    }
    
    public void ShowReward(int valueReward, string description)
    {
        gameObject.SetActive(true);
        _valueRewardTxt.text = valueReward.ToString();
        
        if (_descriptionTxt != null)
            _descriptionTxt.text = description;
    }
}
