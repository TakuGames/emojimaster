﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FatalityController : MonoBehaviour
{
    [SerializeField] private Animator _player;
    [SerializeField] private Animator _opponent;

    public void OnPlay()
    {
        _player.SetBool("GunLightning", true);
        _opponent.SetBool("GunLightning", true);
        //gameObject.SetActive(false);
    }
}
