﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using Random = UnityEngine.Random;

#region Score Struct

[Serializable]
public struct Score
{
    private static readonly Score _zeroScore = new Score(0,0,0);
    private static readonly Score _winScore = new Score(100,50,10);
    private static readonly Score _loseScore = new Score(50,100,0);
        
    public int like;
    public int dislike;
    public int diffScore;

    public static bool operator !=(Score s1, Score s2)
    {
        return !s1.Equals(s2);
    }

    public static bool operator ==(Score s1, Score s2)
    {
        return s1.Equals(s2);
    }

    public static Score operator ++(Score score)
    {
        score.like++;
        score.diffScore++;
        return score;
    }

    public static Score operator --(Score score)
    {
        if (score.diffScore > 0)
        {
            score.dislike++;
            score.diffScore--;
        }

        return score;
    }

    public static bool operator >(Score s1, Score s2)
    {
        return s1.diffScore > s2.diffScore;
    }

    public static bool operator <(Score s1, Score s2)
    {
        return s1.diffScore < s2.diffScore;
    }


    public Score(int like, int dislike)
    {
        this.like = like;
        this.dislike = dislike;
        this.diffScore = Mathf.Clamp(like - dislike, 0, int.MaxValue);
    }
        
    public Score(int like, int dislike, int diffScore)
    {
        this.like = like;
        this.dislike = dislike;
        this.diffScore = diffScore;
    }

    public static Score zero => _zeroScore;
    public static Score win => _winScore;
    public static Score lose => _loseScore;

    public Score(bool random)
    {
        this.like = Random.Range(50, 100);
        this.dislike = Random.Range(0, 50);
        this.diffScore = like - dislike;
    }
}

#endregion

[CreateAssetMenu(fileName = "NewPerson", menuName = "Dataset/Person")]
public class PersonSO : ScriptableObject
{
    #region Nested Classes

    [Serializable]
    public struct Reward
    {
        public int win;
        public int lose;
    }
    
    [Serializable]
    public struct Avatar
    {
        public Color color;
        public Sprite torso;
        public Sprite headIdle;
        public Sprite headJoy;
        public Sprite headAnger;
    }

    [Serializable]
    public struct Power
    {
        public int scoreTarget;
        public int startLikes;
        public int additionsStartLikes;
        public int additionsLikesInMinute;
    }

    [Serializable]
    public class Price
    {
        public int coins = 50;
        public int advertising = 3;
        public bool acquired = false;
    }

    [Serializable]
    public class Replicas
    {
        [SerializeField] private TextAsset _asset;
        private string[] _replicas;

        public string GetRandomReplica()
        {
            if (_replicas == null)
                Validate();

            if (_replicas.Length == 0)
                return "no replicas";

            return _replicas[Random.Range(0, _replicas.Length)];
        }

        public void Validate()
        {
            _replicas = _asset != null
                ? _asset.text.Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries)
                : new string[0];
        }
    }

    #endregion

    public string name;
    public Avatar avatar;
    public Power power;
    public Price price;
    public Reward reward;
    public Replicas goodReplicasPerson;
    public Replicas badReplicasPerson;
    public Replicas goodReplicasOpponent;
    public Replicas badReplicasOpponent;

    public List<Sprite> uniqueGoodStickers;
    public List<Sprite> uniqueBadStickers;
    
    public bool playLike { get; set; }
    public bool playDislike { get; set; }

    [SerializeField] private bool _defeated;
    
    public bool defeated
    {
        get { return _defeated;}
        set { _defeated = value; }
    }
    public Score score { get; set; }

    public void AddScore(ESent sent)
    {
        switch (sent)
        {
            case ESent.Good:
                score++;
                playLike = true;
                return;
            case ESent.Bad:
                score--;
                playDislike = true;
                return;
        }
    }
    
    public ESent lastSent { get; set; }

    public string GetRandomReplicaPerson(ESent sent)
    {
        return sent == ESent.Good ? goodReplicasPerson.GetRandomReplica() : badReplicasPerson.GetRandomReplica();
    }

    public string GetRandomReplicaOpponent(ESent sent)
    {
        return sent == ESent.Good ? goodReplicasOpponent.GetRandomReplica() : badReplicasOpponent.GetRandomReplica();
    }

    public Sprite GetRandomSticker(ESent sent)
    {
        var stickers = sent == ESent.Good ? uniqueGoodStickers : uniqueBadStickers;
        return stickers[Random.Range(0, stickers.Count)];
    }

    private void OnValidate()
    {
        goodReplicasPerson.Validate();
        badReplicasPerson.Validate();
        goodReplicasOpponent.Validate();
        badReplicasOpponent.Validate();
    }
}