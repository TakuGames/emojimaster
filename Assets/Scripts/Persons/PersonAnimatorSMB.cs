﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonAnimatorSMB : StateMachineBehaviour
{
    private PersonSO _person;
    private Score _curDiffScore;
    private readonly int _like = Animator.StringToHash("Like");
    private readonly int _dislike = Animator.StringToHash("Dislike");

    private void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        _person = animator.GetComponent<PersonUI>().person;
        _curDiffScore = _person.score;
    }

    private void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
//        if (_curDiffScore.like != _person.score.like)
//        {
//            animator.SetTrigger(_like);
//            _curDiffScore.like = _person.score.like;
//        }
//        
//        if (_curDiffScore.dislike != _person.score.dislike)
//        {
//            animator.SetTrigger(_dislike);
//            _curDiffScore.dislike = _person.score.dislike;
//        }

        if (_person.playLike)
        {
            _person.playLike = false;
            animator.SetTrigger(_like);
        }

        if (_person.playDislike)
        {
            _person.playDislike = false;
            animator.SetTrigger(_dislike);
        }
    }


    // OnStateEnter is called before OnStateEnter is called on any state inside this state machine
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called before OnStateExit is called on any state inside this state machine
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called before OnStateMove is called on any state inside this state machine
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateIK is called before OnStateIK is called on any state inside this state machine
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMachineEnter is called when entering a state machine via its Entry Node
    //override public void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
    //{
    //    
    //}

    // OnStateMachineExit is called when exiting a state machine via its Exit Node
    //override public void OnStateMachineExit(Animator animator, int stateMachinePathHash)
    //{
    //    
    //}
}
