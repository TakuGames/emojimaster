﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PersonSO))]
public class PersonEditor : Editor
{
    private PersonSO _person;
    private string _curName;
    
    private void OnEnable()
    {
        _person = (PersonSO) target;
        _curName = _person.name;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        return;
        
        serializedObject.Update();
        //base.OnInspectorGUI();
        
        _person.name = EditorGUILayout.TextField("Name",_person.name);
        ChangeNameAsset();
//        _person.goodReplicasPersonAsset = EditorGUILayout.ObjectField("Unique Good Replicas Asset", _person.goodReplicasPersonAsset, typeof(TextAsset)) as TextAsset;
//        _person.badReplicasPersonAsset = EditorGUILayout.ObjectField("Unique BadReplicas Asset", _person.badReplicasPersonAsset, typeof(TextAsset)) as TextAsset;
//        _person.avatar = EditorGUILayout.ObjectField( _person.avatar, typeof(Sprite), false, GUILayout.Width(100), GUILayout.Height(100)) as Sprite;


        var test = serializedObject.FindProperty("goodReplicasPerson");
        
        
        //EditorGUILayout.ObjectField(serializedObject.FindProperty("goodReplicasPerson"), typeof(PersonSO.Replicas));
        
//        _person.uniqueGoodReplicas = GetReplicasFromAsset(_person.goodReplicasPersonAsset);
//        _person.uniqueBadReplicas = GetReplicasFromAsset(_person.badReplicasPersonAsset);
//
//        ShowReplicas(_person.uniqueGoodReplicas, "Good Replicas");
//        ShowReplicas(_person.uniqueBadReplicas, "Bad Replicas");
        
//        ShowStickers(ref _person.uniqueGoodStickers, serializedObject.FindProperty("uniqueGoodStickers"), "Good Stickers");
//        ShowStickers(ref _person.uniqueBadStickers, serializedObject.FindProperty("uniqueBadStickers"), "Bad Stickers");
        
        serializedObject.ApplyModifiedProperties();
    }

    private void ChangeNameAsset()
    {
        if (!String.Equals(_curName, _person.name))
        {
            _curName = _person.name;
            var path = AssetDatabase.GetAssetPath(_person);
            AssetDatabase.RenameAsset(path, _curName);
        }
    }

    private string[] GetReplicasFromAsset(TextAsset asset)
    {
        var text = asset != null
            ? asset.text.Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries)
            : new string[0];
        return text;
    }

    private void ShowReplicas(string[] text, string label)
    {
        if (text.Length != 0)
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);

            EditorGUILayout.LabelField(label, EditorStyles.boldLabel);
            for (int i = 0; i < text.Length; i++)
            {
                EditorGUILayout.LabelField(text[i]);
            }

            EditorGUILayout.EndVertical();
        }
    }

    private void ShowStickers(ref List<Sprite> collection, SerializedProperty stickersProp, string label)
    {
        EditorGUILayout.LabelField(label, EditorStyles.boldLabel);
        EditorGUILayout.BeginHorizontal();
        
        for (int i = 0; i < stickersProp.arraySize; i++)
        {
            EditorGUILayout.BeginVertical();
            var element = stickersProp.GetArrayElementAtIndex(i);
            element.objectReferenceValue = EditorGUILayout.ObjectField(element.objectReferenceValue, typeof(Sprite),
                true, GUILayout.Width(50), GUILayout.Height(50));
            
            if (GUILayout.Button("-", GUILayout.Width(50)))
            {
                collection.Remove((Sprite) element.objectReferenceValue);
            }
            
            EditorGUILayout.EndVertical();
        }

        if (GUILayout.Button("+", GUILayout.Width(50), GUILayout.Height(50)))
        {
            collection.Add(null);
        }
        EditorGUILayout.EndHorizontal();
    }
}