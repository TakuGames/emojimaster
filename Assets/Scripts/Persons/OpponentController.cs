﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpponentController : MonoBehaviour
{
    [SerializeField] private PersonUI _playerUI;
    [SerializeField] private PersonUI _opponentUI;
    private PersonSO _player;
    private PersonSO _opponent;

    private void Start()
    {
        GenerateOpponents();
    }

    private void GenerateOpponents()
    {
        if (StateGame.player == null || StateGame.opponent == null)
            GetRandomPersons();
        
        _player = StateGame.player.personData;
        _opponent = StateGame.opponent.personData;

        _playerUI.person = _player;
        _opponentUI.person = _opponent;
    }

    public PersonSO GetPerson(EDirection curDirection)
    {
        switch (curDirection)
        {
            case EDirection.Left:
                return _player;
            case EDirection.Right:
                return _opponent;
            default:
                return null;
        }
    }

    public PersonSO GetOpponent(EDirection curDirection)
    {
        switch (curDirection)
        {
            case EDirection.Left:
                return _opponent;
            case EDirection.Right:
                return _player;
            default:
                return null;
        }
    }

    private void GetRandomPersons()
    {
        StateGame.SetPlayer(StateGame.data.single.GetRandomPerson(), StateGame.data.single);
        StateGame.SetOpponent(StateGame.data.tested.GetRandomPerson(), StateGame.data.tested);
    }
}