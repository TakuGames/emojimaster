﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonsUISMB : StateMachineBehaviour
{
    private readonly int _idle = Animator.StringToHash("StartIdle");
    private readonly int _empty = Animator.StringToHash("ToStart");

    private int _state;

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        var currentPlayer = animator.GetComponent<PersonUI>();

        if (!currentPlayer)
            return;

        _state = (currentPlayer.person != ScalerCenter.currentPerson) ? _empty : _idle;
        animator.SetTrigger(_state);
    }
}
